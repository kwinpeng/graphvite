## Install dependency

### [FAISS](https://github.com/facebookresearch/faiss)
- Specify the install path of faiss when building graphvite using cmake command option `-D FAISS_PATH=/path/to/faiss`
- Make sure `libfaiss.so` can be found in `LD_LIBRARY_PATH`

### python
- To specify the desired python installation (e.g. miniconda), try to use cmake command option: `-D CMAKE_PREFIX_PATH=/path/to/miniconda `
- pybind11: recommended way `pip install pybind11`

### gflags
- We use gflags provided in external subdir, to change this use cmake command option: `-D WITH_EMBEDED_GFLAGS=OFF`

### glog
- To use glog provided in external subdir, use cmake command option: `-D WITH_EMBEDED_GLOG=ON`

## How to build
We use cmake build system, to build graphvite simply run following commands

- `mkdir build` && `cd build`
- `cmake -D {XXX} ..` (note that the `path` is the install directory which is organized as `path`/include/faiss)
- `make -j4`

## Run test
- prepare data
- cd python/
- modify config-\*.yaml file
- ln -s ../build/lib/libgraphvite.so graphvite.so
- ./run.sh config-\*.yaml
