#include <glog/logging.h>
#include <gflags/gflags.h>

// for full speed
#undef USE_LOSS
#undef USE_TIMER

#include "graph.h"
#include "../include/solver.cuh"

DEFINE_string(device_ids, "", "GPU devices (to simulate more GPUs, repeat the ids)");
DEFINE_int32(num_partition, graphvite::kAuto, "number of partitions (0 for auto)");
DEFINE_int32(num_negative, 5, "number of negative samples per positive sample");
DEFINE_int32(batch_size, 1e5, "number of samples per GPU batch");

DEFINE_double(lr, 0.025, "initial learning rate");
DEFINE_double(weight_decay, 5e-5, "weight decay");
DEFINE_int32(num_epoch, 4000, "number of total training epochs (w.r.t. edges)");
DEFINE_int32(episode_size, 250, "number of batches per episode");
DEFINE_int32(random_walk_step, 10, "number of edges walked from a node");
DEFINE_int32(random_walk_chain_length, 40, "number of edges walked a Markov chain");
DEFINE_int32(random_walk_batch_size, 100, "number of chains per random walk batch");
DEFINE_int32(shuffle_base, graphvite::kAuto, "number of columns in the pseudo shuffle");
DEFINE_double(negative_sample_exponent, 0.75, "exponent for negative sampling");
DEFINE_double(negative_weight, 5, "weight of negative samples (1 = same as positive samples)");
DEFINE_int32(num_sampler_per_worker, 1, "number of samplers per worker");
DEFINE_int32(log_frequency, 1000, "number of batches per log");

DEFINE_string(train_file, "../gpu/youtube-links.txt", "graph file to train");
DEFINE_string(save_file, "vertex_embedding.bin", "embedding file to save");

int main(int argc, char **argv) {
    FLAGS_stderrthreshold = google::INFO;
    google::SetLogDestination(google::INFO, "log/");
    google::InitGoogleLogging(argv[0]);
    gflags::SetUsageMessage("GPU graph embedding test");
    gflags::ParseCommandLineFlags(&argc, &argv, true);
    std::vector<int> device_ids;
    for (auto &&c: FLAGS_device_ids)
        if (c != ',') {
            CHECK(c >= '0' && c <= '7') << "Can't recognize device id `" << c << "`";
            device_ids.push_back(c - '0');
        }

    graphvite::WordGraph<unsigned int> graph;
    graphvite::GraphSolver<128, float, unsigned int> solver(device_ids, FLAGS_num_sampler_per_worker);
    {
        std::chrono::system_clock::time_point start = std::chrono::high_resolution_clock::now();
        graph.load_compact(FLAGS_train_file.c_str());
        std::chrono::system_clock::time_point end = std::chrono::high_resolution_clock::now();
        float time = (end - start).count();
        LOG(INFO) << "load graph: " << time / 1.0e6 << " ms";
    }

    solver.build(graph, FLAGS_num_partition, FLAGS_num_negative, FLAGS_batch_size, FLAGS_episode_size);
}
