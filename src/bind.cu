#include "bind.h"

PYBIND11_MODULE(graphvite, module) {
    py::enum_<DType> pyDType(module, "dtype");
    pyDType.value("float32", DType::float32)
            .value("uint32", DType::uint32)
            .value("uint64", DType::uint64);
    // shortcuts
    module.attr("float32") = pyDType.attr("float32");
    module.attr("uint32") = pyDType.attr("uint32");
    module.attr("uint64") = pyDType.attr("uint64");

    // Optimizers
    auto optimizer = module.def_submodule("optimizer");
    pyOptimizer(optimizer, "Optimizer");
    pySGD(optimizer, "SGD");
    pyMomentum(optimizer, "Momentum");
    pyAdam(optimizer, "Adam");

    // Graphs
    auto graph = module.def_submodule("graph");
    pyGraph<unsigned int>(graph, "Graph");
    pyGraph<unsigned long long>(graph, "Graph");

    pyWordGraph<unsigned int>(graph, "WordGraph");
    pyWordGraph<unsigned long long>(graph, "WordGraph");

    pyKnowledgeGraph<unsigned int>(graph, "KnowledgeGraph");
    pyKnowledgeGraph<unsigned long long>(graph, "KnowledgeGraph");

    pyKNNGraph<unsigned int>(graph, "KNNGraph");
    pyKNNGraph<unsigned long long>(graph, "KNNGraph");

    // Solvers
    auto solver = module.def_submodule("solver");
//    pyGraphSolver<64, float, unsigned int>(solver, "GraphSolver");
//    pyGraphSolver<96, float, unsigned int>(solver, "GraphSolver");
    pyGraphSolver<128, float, unsigned int>(solver, "GraphSolver");
//    pyGraphSolver<256, float, unsigned int>(solver, "GraphSolver");
//    pyGraphSolver<512, float, unsigned int>(solver, "GraphSolver");
//    pyGraphSolver<64, float, unsigned long long>(solver, "GraphSolver");
//    pyGraphSolver<96, float, unsigned long long>(solver, "GraphSolver");
//    pyGraphSolver<128, float, unsigned long long>(solver, "GraphSolver");
//    pyGraphSolver<256, float, unsigned long long>(solver, "GraphSolver");
//    pyGraphSolver<512, float, unsigned long long>(solver, "GraphSolver");

//    pyKnowledgeGraphSolver<64, float, unsigned int>(solver, "KnowledgeGraphSolver");
//    pyKnowledgeGraphSolver<96, float, unsigned int>(solver, "KnowledgeGraphSolver");
//    pyKnowledgeGraphSolver<128, float, unsigned int>(solver, "KnowledgeGraphSolver");
//    pyKnowledgeGraphSolver<256, float, unsigned int>(solver, "KnowledgeGraphSolver");
    pyKnowledgeGraphSolver<512, float, unsigned int>(solver, "KnowledgeGraphSolver");
    pyKnowledgeGraphSolver<1024, float, unsigned int>(solver, "KnowledgeGraphSolver");
    pyKnowledgeGraphSolver<2048, float, unsigned int>(solver, "KnowledgeGraphSolver");
//    pyKnowledgeGraphSolver<64, float, unsigned long long>(solver, "KnowledgeGraphSolver");
//    pyKnowledgeGraphSolver<96, float, unsigned long long>(solver, "KnowledgeGraphSolver");
//    pyKnowledgeGraphSolver<128, float, unsigned long long>(solver, "KnowledgeGraphSolver");
//    pyKnowledgeGraphSolver<256, float, unsigned long long>(solver, "KnowledgeGraphSolver");
//    pyKnowledgeGraphSolver<512, float, unsigned long long>(solver, "KnowledgeGraphSolver");
//    pyKnowledgeGraphSolver<1024, float, unsigned long long>(solver, "KnowledgeGraphSolver");
//    pyKnowledgeGraphSolver<2048, float, unsigned long long>(solver, "KnowledgeGraphSolver");

    pyVisualizationSolver<2, float, unsigned int>(solver, "VisualizationSolver");
//    pyVisualizationSolver<3, float, unsigned int>(solver, "VisualizationSolver");
//    pyVisualizationSolver<2, float, unsigned long long>(solver, "VisualizationSolver");
//    pyVisualizationSolver<3, float, unsigned long long>(solver, "VisualizationSolver");

    // helper functions
    module.def("Optimizer", optimizer_helper(optimizer),
               py::arg("type") = "SGD");
    module.def("Graph", type_helper<DType>(graph, "Graph"),
               py::arg("index_type") = uint32);
    module.def("Graph", type_helper<DType>(graph, "WordGraph"),
               py::arg("index_type") = uint32);
    module.def("KnowledgeGraph", type_helper<DType>(graph, "KnowledgeGraph"),
               py::arg("index_type") = uint32);
    module.def("KNNGraph", type_helper<DType>(graph, "KNNGraph"),
               py::arg("index_type") = uint32);
    module.def("GraphSolver", type_helper<size_t, DType, DType>(solver, "GraphSolver"),
               py::arg("dim"), py::arg("float_type") = float32, py::arg("index_type") = uint32);
    module.def("KnowledgeGraphSolver", type_helper<size_t, DType, DType>(solver, "KnowledgeGraphSolver"),
               py::arg("dim"), py::arg("float_type") = float32, py::arg("index_type") = uint32);
    module.def("VisualizationSolver", type_helper<size_t, DType, DType>(solver, "VisualizationSolver"),
               py::arg("dim"), py::arg("float_type") = float32, py::arg("index_type") = uint32);
}
