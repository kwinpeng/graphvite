# project output
SET(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/lib)
SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/lib)
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)

ADD_LIBRARY(graphvite SHARED bind.cu)
SET_TARGET_PROPERTIES(graphvite PROPERTIES
    EXCLUDE_FROM_ALL true
    CXX_VISIBILITY_PRESET "hidden"
    CUDA_VISIBILITY_PRESET "hidden"
    LINK_FLAGS "-flto"
    PREFIX "")
TARGET_INCLUDE_DIRECTORIES(graphvite PUBLIC
    ${PROJECT_SOURCE_DIR}/include ${PYTHON_INCLUDE_DIRS} ${FAISS_PATH}/include)
TARGET_LINK_DIRECTORIES(graphvite PUBLIC ${FAISS_PATH}/lib)
TARGET_LINK_LIBRARIES(graphvite faiss ${PYTHON_LIBRARIES} pthread curand glog::glog)
TARGET_COMPILE_OPTIONS(graphvite PRIVATE "-Xcompiler=-fno-fat-lto-objects") # -flto

#INSTALL(TARGETS graphvite
#    LIBRARY DESTINATION ${PROJECT_BINARY_DIR}/python
#    PERMISSIONS OWNER_WRITE OWNER_READ OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
