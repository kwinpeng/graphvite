from __future__ import print_function, division

import os
import pickle
import argparse
from collections import defaultdict

import yaml
import numpy as np
from easydict import EasyDict

import torch
import torch.nn as nn

class LinkPredictor(nn.Module):
	def __init__(self, score_function, entity_embeddings, relation_embeddings):
		super(LinkPredictor, self).__init__()
		entity_embeddings = torch.as_tensor(entity_embeddings)
		relation_embeddings = torch.as_tensor(relation_embeddings)
		if isinstance(score_function, str):
			self.score_function = getattr(LinkPredictor, score_function)
		else:
			self.score_function = score_function
		self.entity_embeddings = nn.Embedding.from_pretrained(entity_embeddings, freeze=True)
		self.relation_embeddings = nn.Embedding.from_pretrained(relation_embeddings, freeze=True)

	def forward(self, head_ids, relation_ids, tail_ids):
		return self.score_function(self.entity_embeddings(head_ids),
								   self.relation_embeddings(relation_ids),
								   self.entity_embeddings(tail_ids))

	@staticmethod
	def TransE(heads, relations, tails):
		x = heads + relations - tails
		distance = x.norm(p=1, dim=1)
		return -distance

	@staticmethod
	def RotatE(heads, relations, tails):
		dim = heads.size(1) // 2

		head_re, head_im = heads.view(-1, dim, 2).permute(2, 0, 1)
		tail_re, tail_im = tails.view(-1, dim, 2).permute(2, 0, 1)
		relations = relations[:, :dim]
		relation_re, relation_im = torch.cos(relations), torch.sin(relations)

		x_re = head_re * relation_re - head_im * relation_im - tail_re
		x_im = head_re * relation_im + head_im * relation_re - tail_im
		x = torch.stack([x_re, x_im], dim=0)
		distance = x.norm(p=2, dim=0).sum(dim=1)
		return -distance

	@staticmethod
	def DistMult(heads, relations, tails):
		x = heads * relations * tails
		score = x.sum(dim=1)
		return score

	@staticmethod
	def ComplEx(heads, relations, tails):
		dim = heads.size(1) // 2

		head_re, head_im = heads.view(-1, dim, 2).permute(2, 0, 1)
		tail_re, tail_im = tails.view(-1, dim, 2).permute(2, 0, 1)
		relation_re, relation_im = relations.view(-1, dim, 2).permute(2, 0, 1)

		x_re = head_re * relation_re - head_im * relation_im
		x_im = head_re * relation_im + head_im * relation_re
		x = x_re * tail_re + x_im * tail_im
		score = x.sum(dim=1)
		return score

def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument("-c", "--config", default="config.yaml", type=str)
	parser.add_argument("-d", "--directory", default="", type=str)
	parser.add_argument("-s", "--split", choices=["valid", "test"], default="valid", type=str)
	parser.add_argument("--no-cuda", action="store_true")

	args = parser.parse_args()
	args.cuda = not args.no_cuda and torch.cuda.is_available()
	return args

if __name__ == "__main__":
	args = parse_args()
	with open(args.config) as config_file:
		cfg = EasyDict(yaml.load(config_file))

	name_file = os.path.join(args.directory, "name2id.pkl")
	print("load name mapping from `%s`" % name_file)
	with open(name_file, "r") as fin:
		entity2id = pickle.load(fin)
		relation2id = pickle.load(fin)
	embedding_file = os.path.join(args.directory, "embedding.npz")
	print("load embeddings from `%s`" % embedding_file)
	embedding = np.load(embedding_file)
	entity_embeddings = embedding["entity"]
	relation_embeddings = embedding["relation"]

	hs = []
	rs = []
	ts = []
	test_split = cfg.dataset[args.split]
	print("evaluate on `%s`" % test_split)
	with open(test_split, "r") as fin:
		for line in fin.readlines():
			h, r, t = line.split()
			if (h in entity2id) and (r in relation2id) and (t in entity2id):
				h = entity2id[h]
				r = relation2id[r]
				t = entity2id[t]
				hs.append(h)
				rs.append(r)
				ts.append(t)
	exclude_head = defaultdict(set)
	exclude_tail = defaultdict(set)
	for split in cfg.dataset.values():
		print("exclude triplets in `%s`" % split)
		with open(split, "r") as fin:
			for line in fin.readlines():
				h, r, t = line.split()
				if h in entity2id and r in relation2id and t in entity2id:
					h = entity2id[h]
					r = relation2id[r]
					t = entity2id[t]
					exclude_head[(t, r)].add(h)
					exclude_tail[(h, r)].add(t)

	num_entity = len(entity2id)

	rankings = []
	import time
	start = time.time()

	model = LinkPredictor(cfg.solver.train.model, entity_embeddings, relation_embeddings)
	if args.cuda:
		model = model.cuda()

	rankings = []
	# evaluate
	i = 0
	for h, r, t in zip(hs, rs, ts):
		i += 1
		if i == 3000:
			break
		negatives = list(set(range(num_entity)) - exclude_tail[(h, r)])
		batch_size = len(negatives) + 1

		batch_h = h * torch.ones(batch_size, dtype=torch.long)
		batch_r = r * torch.ones(batch_size, dtype=torch.long)
		batch_t = torch.as_tensor([t] + negatives)
		if args.cuda:
			batch_h = batch_h.cuda()
			batch_r = batch_r.cuda()
			batch_t = batch_t.cuda()

		score = model(batch_h, batch_r, batch_t)
		rankings.append((score >= score[0]).sum().item())

		negatives = list(set(range(num_entity)) - exclude_head[(t, r)])
		batch_size = len(negatives) + 1

		batch_h = torch.as_tensor([h] + negatives)
		batch_r = r * torch.ones(batch_size, dtype=torch.long)
		batch_t = t * torch.ones(batch_size, dtype=torch.long)
		if args.cuda:
			batch_h = batch_h.cuda()
			batch_r = batch_r.cuda()
			batch_t = batch_t.cuda()

		score = model(batch_h, batch_r, batch_t)
		rankings.append((score >= score[0]).sum().item())

	rankings = np.asarray(rankings)

	print("MR: %f" % np.mean(rankings))
	print("MRR: %f" % np.mean(1 / rankings))
	print("HITS@1: %f" % np.mean(rankings <= 1))
	print("HITS@3: %f" % np.mean(rankings <= 3))
	print("HITS@10: %f" % np.mean(rankings <= 10))
	end = time.time()
	print("[time] evaluate: %f s" % (end - start))