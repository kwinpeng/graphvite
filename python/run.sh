#!/bin/bash
#source ~/.bashrc
#conda activate dl

path=$(date -Iseconds | cut -d "+" -f1)

mkdir ${path}

python train.py -c $1 -d ${path}
python evaluate.py -c $1 -d ${path}
