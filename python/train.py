import os
import time
import pickle
import argparse

import yaml
import numpy as np
from easydict import EasyDict

import graphvite as gv

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="config.yaml", type=str)
    parser.add_argument("-d", "--directory", default="", type=str)

    args = parser.parse_args()
    return args

if __name__ == "__main__":
    args = parse_args()
    with open(args.config) as config_file:
        cfg = EasyDict(yaml.load(config_file))

    start = time.time()
    graph = gv.KnowledgeGraph(index_type=gv.uint32)
    graph.load(cfg.dataset.train, **cfg.graph)
    end = time.time()
    print("[time] load graph: %g s" % (end - start))

    solver = gv.KnowledgeGraphSolver(cfg.solver.dim, float_type=gv.float32, index_type=gv.uint32,
                                     device_ids=cfg.solver.device_ids,
                                     num_sampler_per_worker=cfg.solver.num_sampler_per_worker)
    if "optimizer" in cfg.solver:
        cfg.solver.build.optimizer = gv.Optimizer(**cfg.solver.optimizer)
        print("lr = %f" % cfg.solver.build.optimizer.lr)
    solver.build(graph, **cfg.solver.build)

    start = time.time()
    solver.train(**cfg.solver.train)
    end = time.time()
    print("[time] train embedding: %g s" % (end - start))

    name_file = os.path.join(args.directory, "name2id.pkl")
    print("save name mapping to `%s`" % name_file)
    with open(name_file, "w") as fout:
        pickle.dump(graph.entity2id, fout)
        pickle.dump(graph.relation2id, fout)

    embedding_file = os.path.join(args.directory, "embedding")
    print("save embeddings to `%s.npz`" % embedding_file)
    np.savez_compressed(embedding_file, entity=solver.entity_embeddings, relation=solver.relation_embeddings)