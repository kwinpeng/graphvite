#pragma once

#include <string>
#include <functional>

namespace graphvite {

enum OptimizerType {
    kSGD = 0,
    kMomentum,
    kAdam
};

// Optimizers are binded at run-time in CPU codes
// to reduce the size of excutables
class Optimizer {
public:
    int num_moment;
    std::string type;
    float init_lr, lr, weight_decay;
    std::function<float(int, int)> schedule_function = linear_schedule;
    // Union is used here to enable "polymorphism" on GPU
    union {
        // Momentum
        struct {
            float momentum;
        };
        // Adam
        struct {
            float beta1, beta2, epsilon;
        };
    };

    Optimizer(int) : type("Default"), init_lr(0), lr(0) {}
    Optimizer(float _lr = 1e-4) : type("Default"), init_lr(_lr), lr(_lr) {}

    static float linear_schedule(int batch_id, int num_batch) {
        return std::max(1 - float(batch_id) / num_batch, 1e-4f);
    }

    void apply_schedule(int batch_id, int num_batch) {
        lr = init_lr * schedule_function(batch_id, num_batch);
    }

    void print_info() const {
        LOG(INFO) << "optimizer: " << type;
        LOG(INFO) << "learning rate: " << init_lr << ", weight decay: " << weight_decay;
        if (type == "Momentum" || type == "Nesterov")
            LOG(INFO) << "momentum: " << momentum;
        if (type == "Adam")
            LOG(INFO) << "beta1: " << beta1 << ", beta2: " << beta2 << ", epsilon: " << epsilon;
    }

    template<class Float>
    __device__ inline Float sgd_update(Float parameter, Float gradient, Float weight = 1) const {
        return lr * weight * (gradient + weight_decay * parameter);
    }

    template<class Float>
    __device__ inline Float momentum_update(Float parameter, Float gradient, Float &moment1, Float weight = 1) const {
        Float regularized = weight * (gradient + weight_decay * parameter);
        moment1 = momentum * moment1 + (1 - momentum) + regularized;
        return lr * moment1;
    }

    template<class Float>
    __device__ inline Float adam_update(Float parameter, Float gradient, Float &moment1, Float &moment2,
            Float weight = 1) const {
        Float regularized = weight * (gradient + weight_decay * parameter);
        moment1 = beta1 * moment1 + (1 - beta1) * regularized;
        moment2 = beta2 * moment2 + (1 - beta2) * regularized * regularized;
        return lr *  moment1 / (sqrt(moment2) + epsilon);
    }

protected:
    Optimizer(std::string _type, float _lr = 0.025, float _weight_decay = 0, int _num_moment = 0) :
            type(_type), init_lr(_lr), lr(_lr), weight_decay(_weight_decay), num_moment(_num_moment) {}
};

template<class Float, OptimizerType type>
__device__ decltype(&Optimizer::sgd_update<Float>) get_update_function() {
    switch (type) {
        case kSGD:
            return &Optimizer::sgd_update<Float>;
        default:
            return nullptr;
    }
}

template<class Float, OptimizerType type>
__device__ decltype(&Optimizer::momentum_update<Float>) get_update_function_1_moment() {
    switch (type) {
        case kMomentum:
            return &Optimizer::momentum_update<Float>;
        default:
            return nullptr;
    }
}

template<class Float, OptimizerType type>
__device__ decltype(&Optimizer::adam_update<Float>) get_update_function_2_moment() {
    switch (type) {
        case kAdam:
            return &Optimizer::adam_update<Float>;
        default:
            return nullptr;
    }
}

// helper classes
class SGD : public Optimizer {
public:
    SGD(float _lr = 1e-4, float _weight_decay = 0) :
            Optimizer("SGD", _lr, _weight_decay, 0) {}
};

class Momentum : public Optimizer {
public:
    Momentum(float _lr = 1e-4, float _weight_decay = 0, float _momentum = 0.9) :
            Optimizer("Momentum", _lr, _weight_decay, 1) {
        momentum = _momentum;
    }
};

class Adam : public Optimizer {
public:
    Adam(float _lr = 1e-4, float _weight_decay = 0,
         float _beta1 = 0.999, float _beta2 = 0.99999, float _epsilon = 1e-8) :
            Optimizer("Adam", _lr, _weight_decay, 2) {
        beta1 = _beta1;
        beta2 = _beta2;
        epsilon = _epsilon;
    }
};

} // namespace graphvite