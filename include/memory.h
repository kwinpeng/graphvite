#pragma once

#include <utility>
#include <cuda_runtime.h>

#include "util/debug.h"

namespace graphvite {

template<class _T, class _Index = size_t>
class Memory {
public:
    typedef _T Data;
    typedef _Index Index;

    int device_id;
    Index count = 0;
    cudaStream_t stream;
    int *refer_count = nullptr;
    Data *host_ptr = nullptr, *device_ptr = nullptr;

    Memory(int _device_id, Index _count = 0, cudaStream_t _stream = 0) :
            device_id(_device_id), stream(_stream) {
        resize(_count);
    }

    // Shallow copy
    Memory(const Memory &m) :
            device_id(m.device_id), count(m.count), stream(m.stream), refer_count(m.refer_count),
            host_ptr(m.host_ptr), device_ptr(m.device_ptr) {
        if (count)
            (*refer_count)++;
    }

    Memory &operator=(const Memory &) = delete;

    ~Memory() { resize(0); }

    void swap(Memory &m) {
        std::swap(device_id, m.device_id);
        std::swap(count, m.count);
        std::swap(stream, m.stream);
        std::swap(refer_count, m.refer_count);
        std::swap(host_ptr, m.host_ptr);
        std::swap(device_ptr, m.device_ptr);
    }

    __host__ __device__ Data &operator[](Index index) {
#ifdef __CUDA_ARCH__
        return device_ptr[index];
#else
        return host_ptr[index];
#endif
    }

    __host__ __device__ Data &operator[](Index index) const {
#ifdef __CUDA_ARCH__
        return device_ptr[index];
#else
        return host_ptr[index];
#endif
    }

    void resize(Index _count) {
//        LOG(INFO) << "data size = " << sizeof(Data);
//        LOG(INFO) << "resize to count = " << count;

        if (count == _count)
            return;
        if (count && !--(*refer_count)) {
            // LOG(INFO) << "release memory of " << count << " " << typeid(T).name();
            delete refer_count;
            delete [] host_ptr;
            if (device_id != -1) {
                CUDA_CHECK(cudaSetDevice(device_id));
                CUDA_CHECK(cudaFree(device_ptr));
            }
        }
        count = _count;
        if (count) {
            refer_count = new int(1);
            host_ptr = new Data[count];
            if (device_id != -1) {
                CUDA_CHECK(cudaSetDevice(device_id));
                CUDA_CHECK(cudaMalloc(&device_ptr, count * sizeof(Data)));
            }
        }
    }

    void fill(const Data &data, Index _count) {
        resize(_count);
        for (Index i = 0; i < count; i++)
            host_ptr[i] = data;
    }

    void gather(const std::vector<Data> &memory, const std::vector<Index> &mapping) {
        if (!mapping.empty()) {
            resize(mapping.size());
            for (Index i = 0; i < count; i++)
                host_ptr[i] = memory[mapping[i]];
        }
        else {
            resize(memory.size());
            for (Index i = 0; i < count; i++)
                host_ptr[i] = memory[i];
        }
    }

    void scatter(std::vector<Data> &memory, const std::vector<Index> &mapping) {
        if (!mapping.empty()) {
            for (Index i = 0; i < count; i++)
                memory[mapping[i]] = host_ptr[i];
        }
        else {
            for (Index i = 0; i < count; i++)
                memory[i] = host_ptr[i];
        }
    }

    void scatter_add(std::vector<Data> &memory, const std::vector<Index> &mapping) {
        if (!mapping.empty()) {
            for (Index i = 0; i < count; i++)
                memory[mapping[i]] += host_ptr[i];
        }
        else {
            for (Index i = 0; i < count; i++)
                memory[i] += host_ptr[i];
        }
    }

    void scatter_moving_average(std::vector<Data> &memory, const std::vector<Index> &mapping, float beta) {
        if (!mapping.empty()) {
            for (Index i = 0; i < count; i++)
                memory[mapping[i]] = beta * memory[mapping[i]] + (1 - beta) * host_ptr[i];
        }
        else {
            for (Index i = 0; i < count; i++)
                memory[i] += beta * memory[i] + (1 - beta) * host_ptr[i];
        }
    }

    void scatter_sub(std::vector<Data> &memory, const std::vector<Index> &mapping) {
        if (!mapping.empty()) {
            for (Index i = 0; i < count; i++)
                memory[mapping[i]] -= host_ptr[i];
        }
        else {
            for (Index i = 0; i < count; i++)
                memory[i] -= host_ptr[i];
        }
    }

    void to_device(Index copy_count = 0) {
        if (count && device_id != -1) {
            if (!copy_count)
                copy_count = count;
            CUDA_CHECK(cudaSetDevice(device_id));
            CUDA_CHECK(cudaMemcpyAsync(device_ptr, host_ptr, copy_count * sizeof(Data), cudaMemcpyHostToDevice, stream));
            CUDA_CHECK(cudaStreamSynchronize(stream));
        }
    }

    void to_device_async(Index copy_count = 0) {
        if (count && device_id != -1) {
            if (!copy_count)
                copy_count = count;
            CUDA_CHECK(cudaSetDevice(device_id));
            CUDA_CHECK(cudaMemcpyAsync(device_ptr, host_ptr, copy_count * sizeof(Data), cudaMemcpyHostToDevice, stream));
        }
    }

    void to_host(Index copy_count = 0) {
        if (count && device_id != -1) {
            if (!copy_count)
                copy_count = count;
            CUDA_CHECK(cudaSetDevice(device_id));
            CUDA_CHECK(cudaMemcpyAsync(host_ptr, device_ptr, copy_count * sizeof(Data), cudaMemcpyDeviceToHost, stream));
            CUDA_CHECK(cudaStreamSynchronize(stream));
        }
    }

    void to_host_async(Index copy_count = 0) {
        if (count && device_id != -1) {
            if (!copy_count)
                copy_count = count;
            CUDA_CHECK(cudaSetDevice(device_id));
            CUDA_CHECK(cudaMemcpyAsync(host_ptr, device_ptr, copy_count * sizeof(Data), cudaMemcpyDeviceToHost, stream));
        }
    }

    static size_t gpu_memory_demand(int count) {
        return count * sizeof(Data);
    }
};

} // namespace graphvite