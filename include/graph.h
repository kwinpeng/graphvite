#pragma once

#define FILE_OFFSET_BITS 64
#include <cstdio>
#undef FILE_OFFSET_BITS

#include <tuple>
#include <string>
#include <vector>
#include <thread>
#include <unordered_map>
#include <glog/logging.h>
#include <faiss/gpu/GpuIndexFlat.h>
#include <faiss/gpu/StandardGpuResources.h>

#include "util/common.h"

namespace graphvite {

const int kMaxLineLength = 1048576;

namespace { // namespace anonymous
template<class _Index, class ..._Attributes>
class GraphMixin {
    static_assert(std::is_integral<_Index>::value, "GraphMixin can only be instantiated with integral types");
public:
    typedef _Index Index;
    typedef std::tuple<Index, _Attributes...> VertexEdge;
    typedef std::tuple<Index, Index, _Attributes...> Edge;

    std::vector<std::vector<VertexEdge>> vertex_edges;
    std::vector<Edge> edges;
    std::vector<float> vertex_weights, edge_weights;

    Index num_vertex;
    size_t num_edge;

#define USING_GRAPH_MIXIN(type) \
    using typename type::VertexEdge; \
    using typename type::Edge; \
    using type::vertex_edges; \
    using type::edges; \
    using type::vertex_weights; \
    using type::edge_weights; \
    using type::num_vertex; \
    using type::num_edge; \
    using type::print_info

    GraphMixin() = default;
    GraphMixin(const GraphMixin &) = delete;
    GraphMixin &operator=(const GraphMixin &) = delete;

    virtual void reset() {
        num_vertex = 0;
        num_edge = 0;
        vertex_edges.clear();
        edges.clear();
        vertex_weights.clear();
        edge_weights.clear();
    }

    virtual void flatten() = 0;

    virtual inline void print_name() const {
        std::stringstream s;
        auto _ = {(s << ", " << pretty::type_name<_Attributes>(), 0)...};
        LOG(INFO) << "GraphMixin<" << pretty::type_name<Index>() << s.str() << ">";
    }

    virtual inline void print_graph_info() const {
        LOG(INFO) << "#vertex: " << num_vertex << ", #edge: " << num_edge;
    }

    void print_info() {
        LOG(INFO) << pretty::begin;
        print_name();
        LOG(INFO) << pretty::section("Graph");
        print_graph_info();
        LOG(INFO) << pretty::end;
    }
};
} // namespace anonymous

template<class _Index = size_t>
class Graph : public GraphMixin<_Index, float> {
public:
    typedef GraphMixin<_Index, float> Base;
    USING_GRAPH_MIXIN(Base);

    typedef _Index Index;

    std::unordered_map<std::string, Index> name2id;
    std::vector<std::string> id2name;

#define USING_GRAPH(type) \
    USING_GRAPH_MIXIN(type); \
    using type::name2id; \
    using type::id2name

    void reset() {
        Base::reset();
        name2id.clear();
        id2name.clear();
    }

    void flatten() override {
        if (!edges.empty())
            return;
        for (Index i = 0; i < num_vertex; i++)
            for (auto &&vertex_edge : vertex_edges[i]) {
                edges.push_back(std::tuple_cat(std::tie(i), vertex_edge));
                edge_weights.push_back(std::get<1>(vertex_edge));
            }
    }

    virtual inline void print_name() const override {
        LOG(INFO) << "Graph<" << pretty::type_name<Index>() << ">";
    }

    void load(const char *file_name, bool as_undirected = true, char comment = '#') {
        LOG(INFO) << "Loading graph from " << file_name;
        reset();

        FILE *fin = fopen(file_name, "r");
        fseek(fin, 0, SEEK_END);
        size_t fsize = ftell(fin);
        fseek(fin, 0, SEEK_SET);

        char line[kMaxLineLength];
        while (fgets(line, kMaxLineLength, fin) != nullptr) {
            LOG_EVERY_N(INFO, 1e7) << 100.0 * ftell(fin) / fsize << "%";

            if (line[0] == comment || line[0] == '\n')
                continue;
            char *u_name = strtok(line, " \t");
            char *v_name = strtok(nullptr, " \t\r\n");
            char *w_str = strtok(nullptr, " \t\r\n");

            Index u, v;
            float w = w_str ? atof(w_str) : 1;
            auto u_iter = name2id.find(u_name);
            if (u_iter != name2id.end())
                u = u_iter->second;
            else {
                u = num_vertex++;
                name2id[u_name] = u;
                id2name.push_back(u_name);
                vertex_edges.push_back(std::vector<VertexEdge>());
                vertex_weights.push_back(0);
            }
            auto v_iter = name2id.find(v_name);
            if (v_iter != name2id.end())
                v = v_iter->second;
            else {
                v = num_vertex++;
                name2id[v_name] = v;
                id2name.push_back(v_name);
                vertex_edges.push_back(std::vector<VertexEdge>());
                vertex_weights.push_back(0);
            }
            vertex_edges[u].push_back(std::make_tuple(v, w));
            vertex_weights[u] += w;
            if (as_undirected && u != v) {
                vertex_edges[v].push_back(std::make_tuple(u, w));
                vertex_weights[v] += w;
            }
            num_edge++;
        }
        fclose(fin);
        print_info();
    }

    void save(const char *file_name, bool weighted = true, bool anonymous = false) {
        LOG(INFO) << "Saving weighted graph to " << file_name;

        FILE *fout = fopen(file_name, "w");

        for (unsigned long long i = 0; i < num_vertex; i++)
            for (auto &&vertex_edge : vertex_edges[i]) {
                unsigned long long j = std::get<0>(vertex_edge);
                float w = std::get<1>(vertex_edge);
                if (anonymous)
                    fprintf(fout, "%llu\t%llu", i, j);
                else
                    fprintf(fout, "%s\t%s", id2name[i].c_str(), id2name[j].c_str());
                if (weighted)
                    fprintf(fout, "\t%f", w);
                fputc('\n', fout);
            }
        fclose(fout);
    }
};

template<class _Index = size_t>
class WordGraph : public Graph<_Index> {
public:
    typedef Graph<_Index> Base;
    USING_GRAPH(Base);
    using Base::reset;

    typedef _Index Index;

    inline void print_name() const override {
        LOG(INFO) << "WordGraph<" << pretty::type_name<Index>() << ">";
    }

    void load_compact(const char *file_name, int window = 5, int min_count = 5, char comment = '#') {
        LOG(INFO) << "Generating graph from corpus " << file_name;
        reset();

        FILE *fin = fopen(file_name, "r");
        fseek(fin, 0, SEEK_END);
        size_t fsize = ftell(fin);
        fseek(fin, 0, SEEK_SET);

        char line[kMaxLineLength];
        std::vector<Index> frequency;
        while (fgets(line, kMaxLineLength, fin) != nullptr) {
            LOG_EVERY_N(INFO, 1e7) << 50.0 * ftell(fin) / fsize << "%";

            if (line[0] == comment || line[0] == '\r' || line[0] == '\n')
                continue;

            for (char *word = strtok(line, " \t\r\n"); word; word = strtok(nullptr, " \t\r\n")) {
                auto iter = name2id.find(word);
                if (iter != name2id.end())
                    frequency[iter->second]++;
                else {
                    name2id[word] = num_vertex++;
                    id2name.push_back(word);
                    frequency.push_back(1);
                }
            }
        }
        std::vector<std::string> id2name_new;
        name2id.clear();
        num_vertex = 0;
        for (Index i = 0; i < id2name.size(); i++)
            if (frequency[i] >= min_count) {
                auto &word = id2name[i];
                id2name_new.push_back(word);
                name2id[word] = num_vertex++;
                CHECK(id2name_new.size() == num_vertex) << "not equal";
            }
        id2name = id2name_new;
        vertex_edges.resize(num_vertex);
        vertex_weights.resize(num_vertex);

        fseek(fin, 0, SEEK_SET);
        std::vector<std::unordered_map<Index, float>> edge_map(num_vertex);
        std::vector<Index> sentence;

        while (fgets(line, kMaxLineLength, fin) != nullptr) {
            LOG_EVERY_N(INFO, 1e7) << 50 + 50.0 * ftell(fin) / fsize << "%";

            if (line[0] == comment || line[0] == '\r' || line[0] == '\n')
                continue;

            sentence.clear();
            for (char *word = strtok(line, " \t\r\n"); word; word = strtok(nullptr, " \t\r\n")) {
                auto iter = name2id.find(word);
                if (iter != name2id.end()) {
                    sentence.push_back(iter->second);
                    CHECK(iter->second < num_vertex) << "invalid name2id!";
                }
            }
            for (int i = 0; i < sentence.size(); i++)
                for (int j = 1; j <= window; j++) {
                    if (i + j >= sentence.size())
                        break;
                    Index u = sentence[i];
                    Index v = sentence[i + j];
                    auto edge_iter = edge_map[u].find(v);
                    if (edge_iter == edge_map[u].end())
                        edge_map[u][v] = 1;
                    else
                        edge_iter->second++;
                    edge_iter = edge_map[v].find(u);
                    if (edge_iter == edge_map[v].end())
                        edge_map[v][u] = 1;
                    else
                        edge_iter->second++;
                    vertex_weights[u]++;
                    vertex_weights[v]++;
                }
        }
        fclose(fin);
        for (Index i = 0; i < num_vertex; i++) {
            for (auto &&edge : edge_map[i])
                vertex_edges[i].push_back(edge);
            num_edge += edge_map[i].size();
        }
        print_info();
    }

    void load(const char *file_name, int window = 5, int min_count = 5, char comment = '#')
    DEPRECATED("This function consumes 5~10x memory than load_compact()") {
        LOG(INFO) << "Generating graph from corpus " << file_name;
        reset();

        FILE *fin = fopen(file_name, "r");
        fseek(fin, 0, SEEK_END);
        size_t fsize = ftell(fin);
        fseek(fin, 0, SEEK_SET);

        char line[kMaxLineLength];
        std::vector<Index> frequency;

        while (fgets(line, kMaxLineLength, fin) != nullptr) {
            LOG_EVERY_N(INFO, 1e7) << 50.0 * ftell(fin) / fsize << "%";

            if (line[0] == comment || line[0] == '\r' || line[0] == '\n')
                continue;

            for (char *word = strtok(line, " \t\r\n"); word; word = strtok(nullptr, " \t\r\n")) {
                auto iter = name2id.find(word);
                if (iter != name2id.end())
                    frequency[iter->second]++;
                else {
                    name2id[word] = num_vertex++;
                    id2name.push_back(word);
                    frequency.push_back(1);
                }
            }
        }
        std::vector<std::string> id2name_new;
        name2id.clear();
        num_vertex = 0;
        for (Index i = 0; i < id2name.size(); i++)
            if (frequency[i] >= min_count) {
                auto &word = id2name[i];
                id2name_new.push_back(word);
                name2id[word] = num_vertex++;
                CHECK(id2name_new.size() == num_vertex) << "not equal";
            }
        id2name = id2name_new;
        vertex_edges.resize(num_vertex);
        vertex_weights.resize(num_vertex);

        fseek(fin, 0, SEEK_SET);
        std::vector<Index> sentence;

        while (fgets(line, kMaxLineLength, fin) != nullptr) {
            LOG_EVERY_N(INFO, 1e7) << 50 + 50.0 * ftell(fin) / fsize << "%";

            if (line[0] == comment || line[0] == '\r' || line[0] == '\n')
                continue;

            sentence.clear();
            for (char *word = strtok(line, " \t\r\n"); word; word = strtok(nullptr, " \t\r\n")) {
                auto iter = name2id.find(word);
                if (iter != name2id.end()) {
                    sentence.push_back(iter->second);
                    CHECK(iter->second < num_vertex) << "invalid name2id!";
                }
            }
            for (int i = 0; i < sentence.size(); i++)
                for (int j = 1; j <= window; j++) {
                    if (i + j >= sentence.size())
                        break;
                    Index u = sentence[i];
                    Index v = sentence[i + j];
                    vertex_edges[u].push_back(std::make_tuple(v, 1));
                    vertex_edges[v].push_back(std::make_tuple(u, 1));
                    vertex_weights[u]++;
                    vertex_weights[v]++;
                    num_edge++;
                }
        }
        fclose(fin);
        print_info();
    }
};

template<class _Index = size_t>
class KNNGraph : public GraphMixin<_Index, float> {
public:
    typedef GraphMixin<_Index, float> Base;
    USING_GRAPH_MIXIN(Base);

    typedef _Index Index;

    int device_id, num_thread;
    int dim, num_neighbor;
    float perplexity;

    KNNGraph(int _device_id = 0, int _num_thread = 1) :
            device_id(_device_id), num_thread(_num_thread) {}

    void flatten() override {
        if (!edges.empty())
            return;
        for (Index i = 0; i < num_vertex; i++)
            for (auto &&vertex_edge : vertex_edges[i]) {
                edges.push_back(std::tuple_cat(std::tie(i), vertex_edge));
                edge_weights.push_back(std::get<1>(vertex_edge));
            }
    }

    void reset() override {
        Base::reset();
        dim = 0;
    }

    inline void print_name() const override {
        LOG(INFO) << "KNNGraph<" << pretty::type_name<Index>() << ">";
    }

    inline void print_graph_info() const override {
        LOG(INFO) << "#vertex: " << num_vertex << ", #nearest neighbor: " << num_neighbor;
        LOG(INFO) << "perplexity: " << perplexity;
    }

    void normalize_perplexity(Index start, Index end) {
        for (Index i = start; i < end; i++) {
            float entropy;
            float low = 0, high = 80, beta, norm;
            for (int j = 0; j < 100; j++) {
                beta = (low + high) / 2;
                norm = 0;
                for (auto &&vertex_edge : vertex_edges[i]) {
                    float weight = std::get<1>(vertex_edge);
                    norm += exp(-beta * weight);
                    entropy += beta * weight * exp(-beta * weight);
                }
                entropy = entropy / norm + log(norm);
                if (abs(entropy - log(perplexity)) < 1e-5)
                    break;
                if (entropy > log(perplexity))
                    low = beta;
                else
                    high = beta;
            }
            for (auto &&vertex_edge : vertex_edges[i]) {
                float &weight = std::get<1>(vertex_edge);
                weight = exp(-beta * weight) / norm;
            }
            vertex_weights[i] = 1;
        }
    }

    void symmetrize(Index start, Index end) {
        for (Index i = start; i < end; i++)
            for (auto &&edge_i : vertex_edges[i]) {
                Index j = std::get<0>(edge_i);
                if (i < j)
                    for (auto &&edge_j : vertex_edges[j])
                        if (std::get<0>(edge_j) == i) {
                            float weight = (std::get<1>(edge_i) + std::get<1>(edge_j)) / 2;
                            std::get<1>(edge_i) = weight;
                            std::get<1>(edge_j) = weight;
                            break;
                        }
            }
    }

    void load_vector(const char *file_name, int _num_neighbor = 150, float _perplexity = 50, char comment = '#') {
        LOG(INFO) << "Generating KNN graph from " << file_name;
        reset();

        num_neighbor = _num_neighbor;
        perplexity = _perplexity;

        FILE *fin = fopen(file_name, "r");
        fseek(fin, 0, SEEK_END);
        size_t fsize = ftell(fin);
        fseek(fin, 0, SEEK_SET);

        char line[kMaxLineLength];
        std::vector<float> vectors;
        while (fgets(line, kMaxLineLength, fin) != nullptr) {
            LOG_EVERY_N(INFO, 1e7) << 100.0 * ftell(fin) / fsize << "%";

            if (line[0] == comment || line[0] == '\r' || line[0] == '\n')
                continue;

            int current_dim = 0;
            for (char *f_str = strtok(line, " \t\r\n"); f_str; f_str = strtok(nullptr, " \t\r\n")) {
                float f = atof(f_str);
                vectors.push_back(f);
                current_dim++;
            }
            if (!dim)
                dim = current_dim;
            CHECK(current_dim == dim)
                    << "Incompatible #dimension. Expect " << dim << ", but " << current_dim << " is found";
        }
        num_vertex = vectors.size() / dim;

        faiss::gpu::StandardGpuResources resources;
        faiss::gpu::GpuIndexFlatConfig config;
        config.device = device_id;
        faiss::gpu::GpuIndexFlatL2 index(&resources, dim, config);
        std::vector<faiss::Index::distance_t> distances(num_vertex * (num_neighbor + 1));
        std::vector<faiss::Index::idx_t> labels(num_vertex * (num_neighbor + 1));

        LOG(INFO) << "faiss start";
        index.add(num_vertex, vectors.data());
        // faiss returns squared l2 distances
        index.search(num_vertex, vectors.data(), num_neighbor + 1, distances.data(), labels.data());
        LOG(INFO) << "faiss end";

        vertex_edges.resize(num_vertex);
        vertex_weights.resize(num_vertex, 0);
        for (int i = 0; i < num_vertex; i++) {
            vertex_edges[i].resize(num_neighbor);
            for (int j = 0; j < num_neighbor; j++) {
                int k = i * (num_neighbor + 1) + j + 1;
                CHECK(labels[k] >= 0 && labels[k] < num_vertex);
                CHECK(distances[k] >= 0);
                vertex_edges[i][j] = std::make_tuple(labels[k], distances[k]);
                vertex_weights[i] += distances[k];
            }
        }
        num_edge = num_vertex * num_neighbor;

        std::vector<std::thread> threads(num_thread);
        LOG(INFO) << "normalize perplexity";
        Index work_load = (num_vertex + num_thread - 1) / num_thread;
        for (int i = 0; i < num_thread; i++)
            threads[i] = std::thread(&KNNGraph::normalize_perplexity, this, work_load * i,
                                     std::min(work_load * (i + 1), num_vertex));
        for (auto &&thread : threads)
            thread.join();
        LOG(INFO) << "symmetrize";
        for (int i = 0; i < num_thread; i++)
            threads[i] = std::thread(&KNNGraph::symmetrize, this, work_load * i,
                                     std::min(work_load * (i + 1), num_vertex));
        for (auto &&thread : threads)
            thread.join();
        print_info();
    }
};

template<class _Index = size_t>
class KnowledgeGraph : public GraphMixin<_Index, _Index> {
public:
    typedef GraphMixin<_Index, _Index> Base;
    USING_GRAPH_MIXIN(Base);

    typedef _Index Index;

    std::unordered_map<std::string, Index> entity2id, relation2id;
    std::vector<std::string> id2entity, id2relation;
    Index num_relation;

    void reset() override {
        Base::reset();
        entity2id.clear();
        relation2id.clear();
        id2entity.clear();
        id2relation.clear();
    }

    // TODO: seperate normalize from flatten
    void flatten() override {
        if (!edges.empty())
            return;
        std::unordered_map<Index, int> head_count, tail_count;
        for (Index h = 0; h < num_vertex; h++)
            for (auto &&vertex_edge : vertex_edges[h]) {
                Index t = std::get<0>(vertex_edge);
                Index r = std::get<1>(vertex_edge);
                if (head_count.find(h * num_relation + r) == head_count.end())
                    head_count[h * num_relation + r] = 0;
                head_count[h * num_relation + r]++;
                if (tail_count.find(t * num_relation + r) == tail_count.end())
                    tail_count[t * num_relation + r] = 0;
                tail_count[t * num_relation + r]++;
            }
        for (Index h = 0; h < num_vertex; h++)
            for (auto &&vertex_edge : vertex_edges[h]) {
                Index t = std::get<0>(vertex_edge);
                Index r = std::get<1>(vertex_edge);
                edges.push_back(std::tuple_cat(std::tie(h), vertex_edge));
//                edge_weights.push_back(1 / sqrt(head_count[h * num_relation + r] + tail_count[t * num_relation + r] + 8));
                edge_weights.push_back(1 / sqrt((head_count[h * num_relation + r]) * (tail_count[t * num_relation + r])));
//                edge_weights.push_back(1);
            }
    }

    inline void print_name() const override {
        LOG(INFO) << "KnowledgeGraph<" << pretty::type_name<Index>() << ">";
    }

    inline void print_graph_info() const override {
        LOG(INFO) << "#entity: " << num_vertex << ", #relation: " << num_relation;
        LOG(INFO) << "#triplet: " << num_edge;
    }

    void load(const char *file_name, bool undirected_weight = true, char comment = '#') {
        LOG(INFO) << "Loading knowledge graph from " << file_name;
        reset();

        FILE *fin = fopen(file_name, "r");
        fseek(fin, 0, SEEK_END);
        size_t fsize = ftell(fin);
        fseek(fin, 0, SEEK_SET);

        char line[kMaxLineLength];

        while (fgets(line, kMaxLineLength, fin) != nullptr) {
            LOG_EVERY_N(INFO, 1e7) << 100.0 * ftell(fin) / fsize << "%";

            if (line[0] == comment || line[0] == '\n')
                continue;
            char *h_name = strtok(line, " \t");
            char *r_name = strtok(nullptr, " \t");
            char *t_name = strtok(nullptr, "\r\n");

            Index h, t, r;
            auto h_iter = entity2id.find(h_name);
            if (h_iter != entity2id.end())
                h = h_iter->second;
            else {
                h = num_vertex++;
                entity2id[h_name] = h;
                id2entity.push_back(h_name);
                vertex_edges.push_back(std::vector<VertexEdge>());
                vertex_weights.push_back(0);
            }
            auto r_iter = relation2id.find(r_name);
            if (r_iter != relation2id.end())
                r = r_iter->second;
            else {
                r = num_relation++;
                relation2id[r_name] = r;
                id2relation.push_back(r_name);
            }
            auto t_iter = entity2id.find(t_name);
            if (t_iter != entity2id.end())
                t = t_iter->second;
            else {
                t = num_vertex++;
                entity2id[t_name] = t;
                id2entity.push_back(t_name);
                vertex_edges.push_back(std::vector<VertexEdge>());
                vertex_weights.push_back(0);
            }
            vertex_edges[h].push_back(std::make_tuple(t, r));
            vertex_weights[h]++;
            if (undirected_weight)
                vertex_weights[t]++;
            num_edge++;
        }
        fclose(fin);
        print_info();
    }

    void save(const char *file_name, bool anonymous = false) {
        LOG(INFO) << "Saving weighted graph to " << file_name;

        FILE *fout = fopen(file_name, "w");

        for (unsigned long long i = 0; i < num_vertex; i++)
            for (auto &&vertex_edge : vertex_edges[i]) {
                unsigned long long j = std::get<0>(vertex_edge);
                unsigned long long r = std::get<1>(vertex_edge);
                if (anonymous)
                    fprintf(fout, "%llu\t%llu\t%llu\n", i, j, r);
                else
                    fprintf(fout, "%s\t%s\t%s\n", id2entity[i].c_str(), id2entity[j].c_str(), id2relation[r].c_str());
            }
        fclose(fout);
    }
};

#undef USING_GRAPH
#undef USING_GRAPH_MIXIN

} // namespace graphvite