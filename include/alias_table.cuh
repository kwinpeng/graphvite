#pragma once

#include <vector>
#include <memory>
#include <queue>
#include <cuda_runtime.h>

#include "memory.h"

#include <cassert>

namespace graphvite {

template <class Float = float, class Index = size_t>
class AliasTable;

namespace gpu {

template <class Float, class Index>
__global__ void Sample(AliasTable<Float, Index> sampler, Memory<double, int> rand, Memory<Index, int> result);

}

template <class _Float, class _Index>
class AliasTable {
public:
    typedef _Float Float;
    typedef _Index Index;

    int device_id;
    Index count;
    cudaStream_t stream;
    Memory<Float, Index> prob_table;
    Memory<Index, Index> alias_table;

    AliasTable(int _device_id, cudaStream_t _stream = 0) :
            device_id(_device_id), count(0), stream(_stream), prob_table(device_id, 0, stream),
            alias_table(device_id, 0, stream) {}

    // Shallow copy
    AliasTable(const AliasTable &a) :
            device_id(a.device_id), count(a.count), stream(a.stream), prob_table(a.prob_table),
            alias_table(a.alias_table) {}

    AliasTable &operator=(const AliasTable &) = delete;

    void build(const std::vector<Float> &_prob_table) {
        count = _prob_table.size();
        CHECK(count > 0) << "Invalid sampling distribution";
        prob_table.resize(count);
        alias_table.resize(count);

        memcpy(prob_table.host_ptr, _prob_table.data(), count * sizeof(Float));
        // single precision may cause considerable trunctation error
        double norm = 0;
        for (int i = 0; i < count; i++)
            norm += prob_table[i];
        norm = norm / count;
        for (int i = 0; i < count; i++)
            prob_table[i] /= norm;

        std::queue<Index> large, little;
        for (int i = 0; i < count; i++) {
            if (prob_table[i] < 1)
                little.push(i);
            else
                large.push(i);
        }
        while (!little.empty() && !large.empty()) {
            Index i = little.front(), j = large.front();
            little.pop();
            large.pop();
            alias_table[i] = j;
            prob_table[j] = prob_table[i] + prob_table[j] - 1;
            if (prob_table[j] < 1)
                little.push(j);
            else
                large.push(j);
        }
        // suppress trunction error
        while (!little.empty()) {
            Index i = little.front();
            little.pop();
            alias_table[i] = i;
        }
        while (!large.empty()) {
            Index i = large.front();
            large.pop();
            alias_table[i] = i;
        }
    }

    void to_device() {
        prob_table.to_device();
        alias_table.to_device();
    }

    void to_device_async() {
        prob_table.to_device_async();
        alias_table.to_device_async();
    }

    __host__ __device__ inline Index sample(Index index, Float prob) const {
        return prob < prob_table[index] ? index : alias_table[index];
    }

    void device_sample(const Memory<double, int> &rand, Memory<Index, int> *result) {
        int grid_dim = (result->count + 512 - 1) / 512;
        gpu::Sample<Float, Index><<<grid_dim, 512, 0, stream>>>(*this, rand, *result);
    }

    static size_t gpu_memory_demand(int count) {
        size_t demand = 0;
        demand += decltype(prob_table)::gpu_memory_demand(count);
        demand += decltype(alias_table)::gpu_memory_demand(count);
        return demand;
    }
};

namespace gpu {

template <class Float, class Index>
__global__ void Sample(AliasTable<Float, Index> sampler, Memory<double, int> rand, Memory<Index, int> result) {
    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    if (thread_id >= result.count) return;

    Index index = rand[thread_id * 2] * sampler.count;
    Float prob = rand[thread_id * 2 + 1];
    result[thread_id] = sampler.sample(index, prob);
}

} // namespace gpu

} // namespace graphvite