#pragma once

namespace graphvite {
namespace gpu{

const int kBlockPerGrid = 8192;
const int kThreadPerBlock = 512;
const int kWarpSize = 32;
const unsigned kFullMask = 0xFFFFFFFF;

template <class T>
__device__ T WarpReduce(T value) {
#pragma unroll
    for (int delta = 1; delta < kWarpSize; delta *= 2)
#if __CUDACC_VER_MAJOR__ >= 9
        value += __shfl_down_sync(kFullMask, value, delta);
#else
        value += __shfl_down(value, delta);
#endif
    return value;
}

template <class T>
__device__ T WarpBroadcast(T value, int lane_id) {
#if __CUDACC_VER_MAJOR__ >= 9
    return __shfl_sync(kFullMask, value, lane_id);
#else
    return __shfl(value, lane_id);
#endif
}

} // namespace gpu
} // namespace graphvite