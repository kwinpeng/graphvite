#pragma once

#include <string>
#include <chrono>
#include <unordered_map>
#include <glog/logging.h>

namespace graphvite {

class Timer {
public:
	typedef std::chrono::system_clock::time_point time_point;
	typedef std::chrono::high_resolution_clock clock;

	static std::unordered_map<std::string, int> occurrence;

#ifdef USE_TIMER
	const char *prompt;
	int log_frequency;
	time_point start;
#endif

	Timer(const char *_prompt, int _log_frequency = 1)
#ifdef USE_TIMER
		: prompt(_prompt), log_frequency(_log_frequency), start(clock::now()) {
		if (occurrence.find(prompt) == occurrence.end())
			occurrence[prompt] = 0;
	}
#else
	{}
#endif
	~Timer() {
#ifdef USE_TIMER
		time_point end = clock::now();
		LOG_IF(INFO, ++occurrence[prompt] == 1) << prompt << ": " << (end - start).count() / 1.0e6 << " ms";
		occurrence[prompt] %= log_frequency;
#endif
	}
};

std::unordered_map<std::string, int> Timer::occurrence;

} // namespace graphvite