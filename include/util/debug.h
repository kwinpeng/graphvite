#pragma once

#include <glog/logging.h>

namespace graphvite {

#define CUDA_CHECK(error) CudaCheck((error), __FILE__, __LINE__)
#define CURAND_CHECK(error) CurandCheck((error), __FILE__, __LINE__)

inline void CudaCheck(cudaError_t error, const char *file_name, int line) {
    CHECK(error == cudaSuccess)
            << "CUDA error " << cudaGetErrorString(error) << " at " << file_name << ":" << line;
}

inline void CurandCheck(curandStatus_t error, const char *file_name, int line) {
    CHECK(error == CURAND_STATUS_SUCCESS)
            << "CURAND error " << error << " at " << file_name << ":" << line;
}

} // namespace graphvite