#pragma once

#include <string>

namespace graphvite {

namespace pretty {

template <class T>
std::string type_name();

template <>
std::string type_name<float>() { return "float32"; }

template <>
std::string type_name<double>() { return "float64"; }

template <>
std::string type_name<int>() { return "int32"; }

template <>
std::string type_name<unsigned int>() { return "uint32"; }

template <>
std::string type_name<long long>() { return "int64"; }

template <>
std::string type_name<unsigned long long>() { return "uint64"; }

std::string yes_no(bool x) {
    return x ? "yes" : "no";
}

std::string size_string(size_t x) {
    if (x >= 1 << 30)
        return std::to_string(x / float(1 << 30)) + " GB";
    if (x >= 1 << 20)
        return std::to_string(x / float(1 << 20)) + " MB";
    if (x >= 1 << 10)
        return std::to_string(x / float(1 << 10)) + " KB";
    return std::to_string(x) + " B";
}

std::string begin(40, '<');
std::string line(15, '-');
std::string end(40, '>');

inline std::string section(std::string name) {
    std::stringstream s;
    s << line << " " << name << " " << line;
    return s.str();
}

} // namespace pretty
} // namespace graphvite