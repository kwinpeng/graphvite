#pragma once

#include "io.h"

namespace graphvite {

#define DEPRECATED(reason) __attribute__ ((deprecated(reason)))
#define VIRTUAL_ERROR(result) \
    LOG(FATAL) << "No override of " << __PRETTY_FUNCTION__ << " is found"; \
    return result

template <class _T, template<class...> class _Template>
struct is_instantiation : public std::false_type {};

template <template<class...> class _Template, class... Args>
struct is_instantiation<_Template<Args...>, _Template> : public std::true_type {};

const double kEpsilon = 1e-15;
const int kAuto = 0;

constexpr size_t KB(size_t x) {
    return x << 10;
}

constexpr size_t MB(size_t x) {
    return x << 20;
}

constexpr size_t GB(size_t x) {
    return x << 30;
}

} // namespace graphvite