#pragma once

#include <set>
#include <atomic>
#include <thread>
#include <random>
#include <vector>
#include <memory>
#include <utility>
#include <algorithm>
#include <functional>
#include <unordered_map>
#include <initializer_list>
#include <curand.h>
#include <cuda_runtime.h>
#include <glog/logging.h>

#include "graph.h"
#include "memory.h"
#include "vector.h"
#include "optimizer.h"
#include "kernel.cuh"
#include "alias_table.cuh"
#include "util/common.h"
#include "util/gpu.cuh"
#include "util/debug.h"
#include "util/time.h"

namespace graphvite {

std::mt19937 seed;
const int kMaxPartition = 8;
const int kRandBatchSize = 5e6;

// Protocols for scheduling embeddings
typedef unsigned int Protocol;
const Protocol kGlobal = 0x1;
const Protocol kHeadPartition = 0x2;
const Protocol kTailPartition = 0x4;
const Protocol kInPlace = 0x8;
const Protocol kSharedWithPrevious = 0x10;

template<size_t _dim, class _Float, class _Index,
        template<class> class _Graph, template<class> class _Sampler, template<class> class _Worker>
class SolverMixin {
    static_assert(std::is_floating_point<_Float>::value, "GraphVite can only be instantiated with floating point types");
public:
    static const size_t dim = _dim;
    typedef _Float Float;
    typedef _Index Index;

    typedef _Graph<Index> Graph;
    typedef _Sampler<SolverMixin> Sampler;
    typedef _Worker<SolverMixin> Worker;

    typedef typename Sampler::EdgeSample EdgeSample;

    typedef Vector<dim, Float> Vector;

    Graph *graph = nullptr;
    Index num_vertex;
    size_t num_edge;
    int num_moment;
    int num_embedding, num_partition, num_negative, sample_batch_size;
    float negative_sample_exponent, negative_weight;
    int num_epoch, episode_size, batch_size, positive_reuse;
    int log_frequency;
    bool shuffle_partition;
    int assignment_offset;
    std::vector<std::shared_ptr<std::vector<Vector>>> embeddings;
    std::vector<std::shared_ptr<std::vector<std::vector<Vector>>>> moments;
    std::vector<Protocol> protocols;
    bool tied_weights;
    std::vector<std::vector<Index>> head_partitions, tail_partitions;
    std::vector<std::pair<int, Index>> head_locations, tail_locations;
    AliasTable<Float, size_t> edge_table = -1;
    std::vector<std::vector<std::vector<std::vector<EdgeSample>>>> sample_pools;
    int pool_id = 0;
    std::vector<Sampler *> samplers;
    std::vector<Worker *> workers;
    std::string model;
    std::set<std::string> available_models;
    Optimizer optimizer;
    int num_worker, num_sampler, num_sampler_per_worker;
    size_t gpu_memory_limit;
    volatile std::atomic<int> batch_id;
    int num_batch;

#define USING_SOLVER_MIXIN(type) \
    using type::dim; \
    using typename type::Float; \
    using typename type::Index; \
    using typename type::Vector; \
    using typename type::Graph; \
    using typename type::Sampler; \
    using typename type::Worker; \
    using type::num_moment; \
    using type::graph; \
    using type::num_vertex; \
    using type::num_edge; \
    using type::num_embedding; \
    using type::num_partition; \
    using type::num_negative; \
    using type::sample_batch_size; \
    using type::positive_reuse; \
    using type::negative_sample_exponent; \
    using type::negative_weight; \
    using type::num_epoch; \
    using type::episode_size; \
    using type::batch_size; \
    using type::optimizer; \
    using type::num_worker; \
    using type::num_sampler; \
    using type::embeddings; \
    using type::model

    SolverMixin(std::vector<int> device_ids = {}, int _num_sampler_per_worker = 1, size_t _gpu_memory_limit = kAuto) :
            num_sampler_per_worker(_num_sampler_per_worker), gpu_memory_limit(_gpu_memory_limit), batch_id(0) {
        if (device_ids.empty()) {
            CUDA_CHECK(cudaGetDeviceCount(&num_worker));
            CHECK(num_worker) << "No GPU devices found";
            for (int i = 0; i < num_worker; i++)
                device_ids.push_back(i);
        }
        else
            num_worker = device_ids.size();
        num_sampler = num_sampler_per_worker * num_worker;
        if (gpu_memory_limit == kAuto) {
            gpu_memory_limit = GB(32);
            size_t free_memory;
            for (auto &&device_id : device_ids) {
                CUDA_CHECK(cudaSetDevice(device_id));
                CUDA_CHECK(cudaMemGetInfo(&free_memory, nullptr));
                gpu_memory_limit = std::min(gpu_memory_limit, free_memory);
            }
        }
        for (int i = 0; i < num_sampler_per_worker; i++)
            for (auto &&device_id : device_ids)
                samplers.push_back(new Sampler(this, device_id));
        for (auto &&device_id : device_ids)
            workers.push_back(new Worker(this, device_id));
    }

    SolverMixin(const SolverMixin &) = delete;

    SolverMixin &operator=(const SolverMixin &) = delete;

    ~SolverMixin() {
        for (auto &&sampler : samplers)
            delete sampler;
        for (auto &&worker : workers)
            delete worker;
    }

    // should be implemented by derived classes
    virtual std::vector<Protocol> get_protocols() const = 0;
    virtual std::vector<Index> get_shapes() const = 0;
    virtual std::set<std::string> get_available_models() const = 0;
    virtual Optimizer get_default_optimizer() const = 0;

    virtual void build_alias() {}

    virtual std::function<void(Sampler *, int, int)> get_sample_function() {
        graph->flatten();
        edge_table.build(graph->edge_weights);
        return &Sampler::sample;
    }

    virtual void init_embeddings() = 0;

    virtual int get_min_partition() const {
        if (num_worker == 1)
            return num_worker;
        if (tied_weights)
            return num_worker * 2;
        else
            return num_worker;
    }

    void build(Graph &_graph, const Optimizer &_optimizer = kAuto, int _num_partition = kAuto, int _num_negative = 1,
               int _batch_size = 100000, int _episode_size = 250) {
        graph = &_graph;
        optimizer = _optimizer;
        if (optimizer.type == "Default") {
            Optimizer default_optimizer = get_default_optimizer();
            if (optimizer.init_lr > 0)
                default_optimizer.init_lr = optimizer.init_lr;
            optimizer = default_optimizer;
        }
        num_vertex = graph->num_vertex;
        num_edge = graph->num_edge;
        num_moment = optimizer.num_moment;
        num_partition = _num_partition;
        num_negative = _num_negative;
        batch_size = _batch_size;
        episode_size = _episode_size;
        available_models = get_available_models();

        // build embeddings & moments
        protocols = get_protocols();
        num_embedding = protocols.size();
        embeddings.resize(num_embedding);
        moments.resize(num_embedding);

        auto shapes = get_shapes();
        CHECK(shapes.size() == num_embedding) << "The number of shapes must equal to the number of embedding matrices";
        Protocol all = 0;
        tied_weights = false;
        for (int i = 0; i < num_embedding; i++) {
            Protocol protocol = protocols[i];
            CHECK(bool(protocol & kGlobal) + bool(protocol & kHeadPartition) + bool(protocol & kTailPartition) == 1)
                    << "The embedding matrix can be only binded to either global range, head partition "
                    << "or tail partition";
            if (protocol & (kHeadPartition | kTailPartition)) {
                if (shapes[i] == kAuto)
                    shapes[i] = num_vertex;
                else
                    CHECK(shapes[i] == num_vertex)
                            << "The shape for a partitioned embedding matrix must be `graph->num_vertex`";
            }
            CHECK(shapes[i] != kAuto) << "Cannot deduce shape for the " << i << "-th embedding matrix";
            if (protocol & kSharedWithPrevious) {
                CHECK(!(protocol & kGlobal)) << "Shared option on global range is not implemented";
                CHECK(i > 0) << "The first embedding matrix cannot be shared";
                CHECK(shapes[i] == shapes[i - 1])
                        << "The " << i - 1 << "-th and the " << i << "-th matrices are shared, "
                        << "but different shapes are specified";
                tied_weights = tied_weights || ((protocols[i] | protocols[i - 1]) &
                        (kHeadPartition | kTailPartition)) == (kHeadPartition | kTailPartition);
                embeddings[i] = embeddings[i - 1];
                moments[i] = moments[i - 1];
            }
            else {
                embeddings[i] = std::make_shared<std::vector<Vector>>();
                embeddings[i]->resize(shapes[i]);
                moments[i] = std::make_shared<std::vector<std::vector<Vector>>>(num_moment);
                for (int j = 0; j < num_moment; j++)
                    (*moments[i])[j].resize(shapes[i]);
            }
            all |= protocol;
        }
        LOG_IF(WARNING, !(all & kHeadPartition))
                << "No embedding matrix is binded to head partition";
        LOG_IF(WARNING, !(all & kTailPartition))
                << "No embedding matrix is binded to tail partition";
        build_alias();
        
        auto min_partition = get_min_partition();
        if (num_partition == kAuto) {
            for (num_partition = min_partition; num_partition < kMaxPartition; num_partition += min_partition)
                if (gpu_memory_demand(num_vertex, num_worker, num_partition, num_negative, batch_size)
                    < gpu_memory_limit)
                    break;
        }
        else {
            CHECK(num_partition >= min_partition) << "#partition should be no less than " << min_partition;
            LOG_IF(WARNING, num_partition > kMaxPartition)
                    << "It is recommended to use #partition no more than " << kMaxPartition
                    << ", but " << num_partition << " partitions are specified";
        }
        CHECK(gpu_memory_demand(num_vertex, num_worker, num_partition, num_negative, batch_size) < gpu_memory_limit)
                << "Can't satisfy the specified GPU memory limit";

        // shuffle partition to get unbiased moment statistics
        shuffle_partition = (all & kGlobal) && num_moment > 0;
        assignment_offset = 0;

        head_partitions = partition(graph->vertex_weights, num_partition);
        tail_partitions = partition(graph->vertex_weights, num_partition);
        head_locations.resize(num_vertex);
        tail_locations.resize(num_vertex);
        for (int i = 0; i < num_partition; i++)
            for (Index j = 0; j < head_partitions[i].size(); j++) {
                Index head_id = head_partitions[i][j];
                head_locations[head_id] = {i, j};
            }
        for (int i = 0; i < num_partition; i++)
            for (Index j = 0; j < tail_partitions[i].size(); j++) {
                Index tail_id = tail_partitions[i][j];
                tail_locations[tail_id] = {i, j};
            }

        // allocating sample pool is slow
        sample_pools.resize(2);
        for (auto &&sample_pool : sample_pools) {
            sample_pool.resize(num_partition);
            for (auto &&partition_pool : sample_pool) {
                partition_pool.resize(num_partition);
                for (auto &&pool : partition_pool)
                    pool.resize(episode_size * batch_size);
            }
        }

        for (auto &&sampler : samplers)
            sampler->build();
        for (auto &&worker : workers)
            worker->build();
    }

    virtual inline void print_name() const {
        LOG(INFO) << "SolverMixin<" << dim << ", "
                  << pretty::type_name<Float>() << ", " << pretty::type_name<Index>() << ">";
    }

    virtual inline void print_resource_info() const {
        LOG(INFO) << "#worker: " << num_worker << ", #sampler: " << num_sampler << ", #partition: " << num_partition;
        LOG(INFO) << "tied weights: " << pretty::yes_no(tied_weights) << ", episode size: " << episode_size;
        LOG(INFO) << "gpu memory limit: " << pretty::size_string(gpu_memory_limit);
    }

    virtual inline void print_sampling_info() const {
        LOG(INFO) << "positive sample batch size: " << sample_batch_size;
        LOG(INFO) << "#negative: " << num_negative << ", negative sample exponent: " << negative_sample_exponent;
    }

    virtual inline void print_training_info() const {
        LOG(INFO) << "model: " << model;
        optimizer.print_info();
        LOG(INFO) << "#epoch: " << num_epoch << ", batch size: " << batch_size;
        LOG(INFO) << "positive reuse: " << positive_reuse << ", negative weight: " << negative_weight;
    }

    void print_info() const {
        LOG(INFO) << pretty::begin;
        print_name();
        LOG(INFO) << pretty::section("Resource");
        print_resource_info();
        LOG(INFO) << pretty::section("Sampling");
        print_sampling_info();
        LOG(INFO) << pretty::section("Training");
        print_training_info();

        LOG(INFO) << pretty::end;
    }

    virtual std::vector<std::vector<std::pair<int, int>>> get_schedule() const {
        if (num_partition == 1)
            return {{{0, 0}}};

        std::vector<std::vector<std::pair<int, int>>> schedule;
        std::vector<std::pair<int, int>> assignment(num_worker);

        if (tied_weights)
            for (int x = 0; x < num_partition; x += num_worker * 2)
                for (int y = 0; y < num_partition; y += num_worker * 2) {
                    // diagonal
                    for (int i = 0; i < num_worker; i++) {
                        int head_partition_id = x + i;
                        int tail_partition_id = y + i;
                        assignment[i] = {head_partition_id, tail_partition_id};
                    }
                    schedule.push_back(assignment);
                    for (int i = 0; i < num_worker; i++) {
                        int head_partition_id = x + num_worker + i;
                        int tail_partition_id = y + num_worker + i;
                        assignment[i] = {head_partition_id, tail_partition_id};
                    }
                    schedule.push_back(assignment);
                    for (int group_size = 1; group_size <= num_worker; group_size *= 2)
                        for (int offset = 0; offset < group_size; offset++) {
                            for (int i = 0; i < num_worker; i++) {
                                int head_partition_id = x + (i / group_size * 2) * group_size + i % group_size;
                                int tail_partition_id = y + (i / group_size * 2 + 1) * group_size
                                                        + (i + offset) % group_size;
                                assignment[i] = {head_partition_id, tail_partition_id};
                            }
                            schedule.push_back(assignment);
                            for (int i = 0; i < num_worker; i++)
                                std::swap(assignment[i].first, assignment[i].second);
                            schedule.push_back(assignment);
                        }
                }
        else
            for (int x = 0; x < num_partition; x += num_worker)
                for (int y = 0; y < num_partition; y += num_worker)
                    for (int offset = 0; offset < num_worker; offset++) {
                        for (int i = 0; i < num_worker; i++) {
                            int head_partition_id = x + (i + offset) % num_partition;
                            int tail_partition_id = y + i;
                            assignment[i] = {head_partition_id, tail_partition_id};
                        }
                        schedule.push_back(assignment);
                    }

        return schedule;
    }

    void train(std::string _model, int _num_epoch = 100, int _sample_batch_size = 2000, int _positive_reuse = 1,
               float _negative_sample_exponent = 0.75, float _negative_weight = 5, int _log_frequency = 1000) {
        model = _model;
        CHECK(available_models.find(model) != available_models.end()) << "Invalid model `" << model << "`";
        num_epoch = _num_epoch;
        sample_batch_size = _sample_batch_size;
        positive_reuse = _positive_reuse;
        negative_sample_exponent = _negative_sample_exponent;
        negative_weight = _negative_weight;
        log_frequency = _log_frequency;

        print_info();
        init_embeddings();
        for (int i = 0; i < num_embedding; i++) {
            if (protocols[i] & kSharedWithPrevious)
                continue;
            for (int j = 0; j < num_moment; j++)
                for (auto &&moment : (*moments[i])[j])
                    moment = 0;
        }

        batch_id = 0;
        num_batch = num_epoch * num_edge / batch_size;

        std::vector<std::thread> sample_threads(num_sampler);
        std::vector<std::thread> worker_threads(num_worker);
        int num_sample = episode_size * batch_size;
        int work_load = (num_sample + num_sampler - 1) / num_sampler;
        auto sample_function = get_sample_function();
        {
            Timer timer("Sample threads");
            for (int i = 0; i < num_sampler; i++)
                sample_threads[i] = std::thread(sample_function, samplers[i], work_load * i,
                                                std::min(work_load * (i + 1), num_sample));
            for (auto &&thread : sample_threads)
                thread.join();
        }
        auto schedule = get_schedule();
        while (batch_id < num_batch) {
            pool_id ^= 1;
            if (shuffle_partition)
                assignment_offset = (assignment_offset + 1) % num_partition;
            // sample function here
//            LOG(INFO) << "end launch following sampling";
            for (auto &&assignment : schedule) {
                for (int i = 0; i < assignment.size(); i++)
                    worker_threads[i] = std::thread(&Worker::train, workers[i],
                                                    assignment[i].first,
                                                    (assignment[i].second + assignment_offset) % num_partition);
//                    workers[0]->train(assignment[i].first, assignment[i].second);
                for (int i = 0; i < assignment.size(); i++)
                    worker_threads[i].join();
            }
            for (int i = 0; i < num_sampler; i++)
                sample_threads[i] = std::thread(sample_function, samplers[i], work_load * i,
                                                std::min(work_load * (i + 1), num_sample));
            {
                Timer timer("Wait for sample threads");
                for (auto &&thread : sample_threads)
                    thread.join();
            }
        }
        for (int i = 0; i < num_worker; i++)
            worker_threads[i] = std::thread(&Worker::write_back, workers[i]);
        for (auto &&thread : worker_threads)
            thread.join();
    }

    static size_t gpu_memory_demand(Index num_vertex, int num_worker = 1, int num_partition = 1, int num_negative = 5,
                                    int batch_size = 100000) {
        Index num_vertex_per_worker = (num_vertex + num_partition - 1) / num_partition;
        return Worker::gpu_memory_demand(num_vertex_per_worker, num_vertex_per_worker, num_negative, batch_size);
    }

private:
    static std::vector<std::vector<Index>> partition(const std::vector<Float> &weights, int num_partition) {
        std::vector<Index> indexes(weights.size());
        for (Index i = 0; i < indexes.size(); i++)
            indexes[i] = i;
        std::sort(indexes.begin(), indexes.end(),
                  [&weights](Index x, Index y) { return weights[x] > weights[y]; });
        std::vector<std::vector<Index>> parts(num_partition);
        for (Index i = 0; i < indexes.size(); i++) {
            Index index = indexes[i];
            int part_id = i % (num_partition * 2);
            part_id = std::min(part_id, num_partition * 2 - 1 - part_id);
            parts[part_id].push_back(index);
        }
        return parts;
    }
};

template<class _Solver, class ..._Attributes>
class SamplerMixin {
public:
    typedef _Solver Solver;
    typedef typename Solver::Float Float;
    typedef typename Solver::Index Index;

    typedef std::tuple<_Attributes...> Attributes;
    typedef std::tuple<Index, Index, _Attributes...> EdgeSample;
    typedef typename Solver::Graph::Edge Edge;

    Solver *solver;
    int device_id;
    cudaStream_t stream;
    Memory<double, int> random;
    curandGenerator_t generator;
    int pool_size;

#define USING_SAMPLER_MIXIN(type) \
    using typename type::Solver; \
    using typename type::Float; \
    using typename type::Index; \
    using typename type::Attributes; \
    using typename type::Edge; \
    using type::solver; \
    using type::device_id; \
    using type::stream; \
    using type::random; \
    using type::generator; \
    using type::pool_size

    virtual Attributes read_attributes(const Edge &edge) const = 0;

    SamplerMixin(Solver *_solver, int _device_id) :
            solver(_solver), device_id(_device_id), random(device_id) {
        CUDA_CHECK(cudaSetDevice(device_id));

        CUDA_CHECK(cudaStreamCreate(&stream));

        random.stream = stream;
        CURAND_CHECK(curandCreateGenerator(&generator, CURAND_RNG_PSEUDO_DEFAULT));
        std::uniform_int_distribution<unsigned long long> random_seed(0, ULLONG_MAX);
        CURAND_CHECK(curandSetPseudoRandomGeneratorSeed(generator, random_seed(seed)));
        CURAND_CHECK(curandSetStream(generator, stream));
    }

    void build() {
        pool_size = solver->sample_pools[0][0][0].size();

        random.resize(kRandBatchSize);
        CURAND_CHECK(curandGenerateUniformDouble(generator, random.device_ptr, kRandBatchSize));
    }

    void sample(int start, int end) {
        CUDA_CHECK(cudaSetDevice(device_id));

        random.to_host();
        CURAND_CHECK(curandGenerateUniformDouble(generator, random.device_ptr, kRandBatchSize));

        auto &sample_pool = solver->sample_pools[solver->pool_id];
        std::vector<std::vector<int>> offsets(solver->num_partition);
        for (auto &&partition_offsets : offsets)
            partition_offsets.resize(solver->num_partition, start);
        int num_complete = 0, rand_id = 0;
        std::vector<std::pair<int, Index>> heads(solver->sample_batch_size);
        std::vector<std::pair<int, Index>> tails(solver->sample_batch_size);
        std::vector<Attributes> attributes(solver->sample_batch_size);
        while (num_complete < solver->num_partition * solver->num_partition) {
            for (int i = 0; i < solver->sample_batch_size; i++) {
                if (rand_id > kRandBatchSize - 2) {
                    random.to_host();
                    CURAND_CHECK(curandGenerateUniformDouble(generator, random.device_ptr, kRandBatchSize));
                    rand_id = 0;
                }
                size_t index = random[rand_id++] * solver->edge_table.count;
                Float prob = random[rand_id++];
                size_t edge_id = solver->edge_table.sample(index, prob);

                Index head_global_id = std::get<0>(solver->graph->edges[edge_id]);
                Index tail_global_id = std::get<1>(solver->graph->edges[edge_id]);
                heads[i] = solver->head_locations[head_global_id];
                tails[i] = solver->tail_locations[tail_global_id];
                attributes[i] = read_attributes(solver->graph->edges[edge_id]);
            }
            for (int i = 0; i < solver->sample_batch_size; i++) {
                int head_partition_id = heads[i].first;
                int tail_partition_id = tails[i].first;
                int &offset = offsets[head_partition_id][tail_partition_id];
                if (offset < end) {
                    auto &pool = sample_pool[head_partition_id][tail_partition_id];
                    Index head_local_id = heads[i].second;
                    Index tail_local_id = tails[i].second;
//                    pool[offset] = std::tie(head_local_id, tail_local_id);
                    pool[offset] = std::tuple_cat(std::tie(head_local_id, tail_local_id), attributes[i]);
//                    LOG_EVERY_N(INFO, solver->episode_size * solver->batch_size) << "(" << head_local_id << ", " << tail_local_id << ", " << std::get<0>(attributes[i]) << ") "
//                                             << "(" << std::get<0>(pool[offset]) << ", " << std::get<1>(pool[offset]) << ", "
//                                             << std::get<2>(pool[offset]) << ")";
                    if (++offset == end)
                        num_complete++;
                }
            }
        }
    }

    static size_t gpu_memory_demand() {
        size_t demand = 0;
        demand += decltype(random)::gpu_memory_demand(kRandBatchSize);
        return demand;
    }
};

template<class _Solver>
class WorkerMixin {
public:
    typedef _Solver Solver;
    typedef typename Solver::Float Float;
    typedef typename Solver::Index Index;
    typedef typename Solver::Vector Vector;
    typedef typename Solver::EdgeSample EdgeSample;

    static const int sample_size = sizeof(EdgeSample) / sizeof(Index);

    Solver *solver;
    int device_id;
    Optimizer optimizer;
    cudaStream_t work_stream, sample_stream;
    Index head_partition_size, tail_partition_size;
    std::vector<std::shared_ptr<Memory<Vector, Index>>> embeddings, gradients;
    std::vector<std::shared_ptr<std::vector<Memory<Vector, Index>>>> moments;
    int head_partition_id, tail_partition_id;
    std::vector<Index> head_global_ids, tail_global_ids;
    Protocol sampler_protocol;
    AliasTable<Float, Index> negative_sampler;
    Memory<Index, int> batch, negative_batch;
#ifdef USE_LOSS
    Memory<Float, int> loss;
#endif
    Memory<double, int> random;
    curandGenerator_t generator;
    int num_moment;
    int num_embedding, num_negative, batch_size;
    int log_frequency;

#define USING_WORKER_MIXIN(type) \
    using typename type::Solver; \
    using typename type::Float; \
    using typename type::Index; \
    using typename type::Vector; \
    using type::solver; \
    using type::work_stream; \
    using type::head_partition_size; \
    using type::tail_partition_size; \
    using type::head_partition_id; \
    using type::tail_partition_id; \
    using type::head_global_ids; \
    using type::tail_global_ids; \
    using type::negative_sampler; \
    using type::batch; \
    using type::negative_batch; \
    using type::batch_size; \
    using type::embeddings; \
    using type::moments; \
    using type::gradients; \
    using type::num_moment; \
    using type::optimizer

    WorkerMixin(Solver *_solver, int _device_id) :
            solver(_solver), device_id(_device_id), negative_sampler(device_id),
            batch(device_id), negative_batch(device_id),
#ifdef USE_LOSS
            loss(device_id),
#endif
            random(device_id) {
        CUDA_CHECK(cudaSetDevice(device_id));

        CUDA_CHECK(cudaStreamCreate(&work_stream));
        CUDA_CHECK(cudaStreamCreate(&sample_stream));

        // work stream
        batch.stream = work_stream;
        negative_batch.stream = work_stream;
#ifdef USE_LOSS
        loss.stream = work_stream;
#endif
        // sample stream
        negative_sampler.stream = sample_stream;
        random.stream = sample_stream;
        CURAND_CHECK(curandCreateGenerator(&generator, CURAND_RNG_PSEUDO_DEFAULT));
        std::uniform_int_distribution<unsigned long long> random_seed(0, ULLONG_MAX);
        CURAND_CHECK(curandSetPseudoRandomGeneratorSeed(generator, random_seed(seed)));
        CURAND_CHECK(curandSetStream(generator, sample_stream));
    }

    WorkerMixin(const WorkerMixin &) = delete;

    WorkerMixin &operator=(const WorkerMixin &) = delete;

    // should be implemented by derived classes
    virtual Protocol get_sampler_protocol() const = 0;
    virtual bool kernel_dispatch() = 0;

    virtual void build_negative_sampler() {
        std::vector <Float> negative_weights;
        if (sampler_protocol & kHeadPartition)
            for (auto &&head_global_id : head_global_ids)
                negative_weights.push_back(std::pow(solver->graph->vertex_weights[head_global_id],
                                                    solver->negative_sample_exponent));
        if (sampler_protocol & kTailPartition)
            for (auto &&tail_global_id : tail_global_ids)
                negative_weights.push_back(std::pow(solver->graph->vertex_weights[tail_global_id],
                                                    solver->negative_sample_exponent));
        negative_sampler.build(negative_weights);
    }

    void build() {
        num_embedding = solver->num_embedding;
        num_moment = solver->num_moment;
        num_negative = solver->num_negative;
        batch_size = solver->batch_size;
        optimizer = solver->optimizer;

        embeddings.clear();
        gradients.clear();
        embeddings.resize(num_embedding);
        gradients.resize(num_embedding);
        moments.resize(num_embedding);
        for (int i = 0; i < num_embedding; i++) {
            embeddings[i] = std::make_shared<Memory<Vector, Index>>(device_id, 0, work_stream);
            gradients[i] = std::make_shared<Memory<Vector, Index>>(device_id, 0, work_stream);
            moments[i] = std::make_shared<std::vector<Memory<Vector, Index>>>();
            for (int j = 0; j < num_moment; j++)
                moments[i]->push_back(Memory<Vector, Index>(device_id, 0, work_stream));
        }

        batch.resize(batch_size * sample_size);
        negative_batch.resize(batch_size * num_negative);
#ifdef USE_LOSS
        loss.resize(batch_size);
#endif
        random.resize(batch_size * num_negative * 2);

        sampler_protocol = get_sampler_protocol();

        head_partition_id = -1;
        tail_partition_id = -1;
    }

    void load_cache_one(int id) {
        Protocol protocol = solver->protocols[id];
        if ((protocol & kSharedWithPrevious) && head_partition_id == tail_partition_id) {
            embeddings[id] = embeddings[id - 1];
            gradients[id] = gradients[id - 1];
            moments[id] = moments[id - 1];
            return;
        }
        if (id > 0 && embeddings[id] == embeddings[id - 1]) {
            embeddings[id] = std::make_shared<Memory<Vector, Index>>(device_id, 0, work_stream);
            gradients[id] = std::make_shared<Memory<Vector, Index>>(device_id, 0, work_stream);
            moments[id] = std::make_shared<std::vector<Memory<Vector, Index>>>();
            for (int i = 0; i < num_moment; i++)
                moments[id]->push_back(Memory<Vector, Index>(device_id, 0, work_stream));
        }
//        LOG(INFO) << "load embedding " << id << " to GPU " << device_id;
        auto &global_embedding = *(solver->embeddings[id]);
        auto &global_moment = *(solver->moments[id]);
        auto &embedding = *embeddings[id];
        auto &moment = *moments[id];
        auto &gradient = *gradients[id];
        std::vector<Index> none, *mapping = &none;

        if (protocol & kHeadPartition)
            mapping = &head_global_ids;
        if (protocol & kTailPartition)
            mapping = &tail_global_ids;

        embedding.gather(global_embedding, *mapping);
        embedding.to_device_async();
        // only load partitioned moments
        // or global moments if uninitialized
        if (!(protocol & kGlobal) || (num_moment && moment[0].count == 0))
            for (int i = 0; i < num_moment; i++) {
                moment[i].gather(global_moment[i], *mapping);
                moment[i].to_device_async();
            }
        if (!(protocol & kInPlace)) {
            gradient.fill(0, embedding.count);
            gradient.to_device_async();
        }
    }

    void write_back_one(int id) {
        Protocol protocol = solver->protocols[id];
        if (id > 0 && embeddings[id] == embeddings[id - 1])
            return;
//        LOG(INFO) << "write back embedding " << id << " from GPU " << device_id;
        auto &global_embedding = *(solver->embeddings[id]);
        auto &global_moment = *(solver->moments[id]);
        auto &embedding = *embeddings[id];
        auto &moment = *moments[id];
        auto &gradient = *gradients[id];
        std::vector<Index> none, *mapping = &none;

        if (protocol & kHeadPartition)
            mapping = &head_global_ids;
        if (protocol & kTailPartition)
            mapping = &tail_global_ids;

        if (protocol & kInPlace) {
            embedding.to_host();
            embedding.scatter(global_embedding, *mapping);
        }
        else {
            gradient.to_host();
            gradient.scatter_sub(global_embedding, *mapping);
        }
        // only write back partitioned moments
        if (!(protocol & kGlobal))
            for (int i = 0; i < num_moment; i++) {
                moment[i].to_host();
                moment[i].scatter(global_moment[i], *mapping);
            }
    }

    void load_cache(int _head_partition_id, int _tail_partition_id) {
//        LOG(INFO) << "load cache (" << _head_partition_id << ", " << _tail_partition_id << ") to GPU " << device_id;
        // check hit/miss
        bool cold_cache = head_partition_id == -1 || tail_partition_id == -1;
        std::vector<bool> hit(num_embedding, false);
        if (!cold_cache) {
            for (int i = 0; i < num_embedding; i++) {
                Protocol protocol = solver->protocols[i];
                if (protocol & kSharedWithPrevious && !hit[i - 1]) {
                    // check swap hit
                    if (head_partition_id == _tail_partition_id && tail_partition_id == _head_partition_id) {
                        embeddings[i].swap(embeddings[i - 1]);
                        gradients[i].swap(gradients[i - 1]);
                        moments[i].swap(moments[i - 1]);
                        hit[i] = true;
                        hit[i - 1] = true;
                    }
                }
                if (!(i > 0 && embeddings[i] == embeddings[i - 1] && !hit[i - 1])){
                    hit[i] = hit[i] || ((protocol & kHeadPartition) && head_partition_id == _head_partition_id);
                    hit[i] = hit[i] || ((protocol & kTailPartition) && tail_partition_id == _tail_partition_id);
                }
            }
            for (int i = 0; i < num_embedding; i++)
                if (!hit[i])
                    write_back_one(i);
        }
        bool sampler_hit = (sampler_protocol & kHeadPartition) && (sampler_protocol & kTailPartition) &&
                           head_partition_id == _tail_partition_id && _head_partition_id == tail_partition_id;
        sampler_hit = sampler_hit || ((sampler_protocol & (kHeadPartition | kTailPartition)) == kHeadPartition
                                      && head_partition_id == _head_partition_id);
        sampler_hit = sampler_hit || ((sampler_protocol & (kHeadPartition | kTailPartition)) == kTailPartition
                                      && tail_partition_id == _tail_partition_id);

        // load cache
        if (head_partition_id != _head_partition_id) {
            head_partition_id = _head_partition_id;
            head_global_ids = solver->head_partitions[head_partition_id];
            head_partition_size = head_global_ids.size();
        }
        if (tail_partition_id != _tail_partition_id) {
            tail_partition_id = _tail_partition_id;
            tail_global_ids = solver->tail_partitions[tail_partition_id];
            tail_partition_size = tail_global_ids.size();
        }
//        LOG(INFO) << "device = " << device_id << ", (" << head_partition_id << ", " << tail_partition_id << "), size = ("
//                  << head_partition_size << ", " << tail_partition_size << ")";
        if (!sampler_hit) {
//            LOG(INFO) << "sampler miss on GPU " << device_id
//                      << ", head = " << head_partition_id << ", tail = " << tail_partition_id;
            build_negative_sampler();
            negative_sampler.to_device_async();
        }
        for (int i = 0; i < num_embedding; i++)
            if (!hit[i])
                load_cache_one(i);
    }

    void write_back() {
        bool cold_cache = head_partition_id == -1 || tail_partition_id == -1;
        if (cold_cache)
            return;
        for (int i = 0; i < num_embedding; i++)
            write_back_one(i);
    }

    void train(int _head_partition_id, int _tail_partition_id) {
//        LOG(INFO) << "start train" << _head_partition_id << ", " << _tail_partition_id;
        CUDA_CHECK(cudaSetDevice(device_id));
        load_cache(_head_partition_id, _tail_partition_id);

        auto &samples = solver->sample_pools[solver->pool_id][head_partition_id][tail_partition_id];
        log_frequency = solver->log_frequency;
        for (int i = 0; i < solver->positive_reuse; i++)
            for (int j = 0; j < solver->episode_size; j++) {
                LOG_EVERY_N(INFO, log_frequency) << "Batch id: " << solver->batch_id;
                memcpy(batch.host_ptr, &samples[j * batch_size], batch_size * sample_size * sizeof(Index));
                train_batch(solver->batch_id++);
            }
//        LOG(INFO) << "end train";
    }

    void train_batch(int batch_id) {
        Timer batch_timer("Batch", log_frequency);
        batch.to_device_async();
        // Sampling
        {
            Timer timer("Sampling", log_frequency);
            int num_sample = batch_size * num_negative;
            {
                Timer timer("Random", log_frequency);
                CURAND_CHECK(curandGenerateUniformDouble(generator, random.device_ptr, num_sample * 2));
                negative_sampler.device_sample(random, &negative_batch);
            }
            CUDA_CHECK(cudaStreamSynchronize(sample_stream));
        }
#ifdef USE_LOSS
        // Loss (last batch)
        {
            Timer timer("Loss", log_frequency);
            loss.to_host();
            Float batch_loss = 0;
            for (int i = 0; i < batch_size; i++)
                batch_loss += loss[i];
            LOG_EVERY_N(INFO, log_frequency)
                    << "loss = " << batch_loss / batch_size;
        }
#endif
        // Train
        {
            Timer timer("Train", log_frequency);
            optimizer.apply_schedule(batch_id, solver->num_batch);
            LOG_IF(FATAL, !kernel_dispatch())
                    << "Can't find a kernel implementation of `" << solver->model << " with " << optimizer.type;
        }
    }

    static size_t gpu_memory_demand(Index num_vertex, Index num_context, int num_negative, int batch_size) {
        // rewrite
        size_t demand = 0;
        /*
        demand += decltype(vertex_cache)::gpu_memory_demand(num_vertex);
        demand += decltype(context_cache)::gpu_memory_demand(num_context);
        demand += decltype(negative_sampler)::gpu_memory_demand(num_context);
        demand += decltype(batch)::gpu_memory_demand(batch_size * 2);
        demand += decltype(negative_batch)::gpu_memory_demand(batch_size * num_negative);
#ifdef USE_LOSS
        demand += decltype(loss)::gpu_memory_demand(batch_size);
#endif
        demand += decltype(random)::gpu_memory_demand(batch_size * num_negative * 2);*/
        return demand;
    }
};

// ------------------- Graph -------------------

template <size_t _dim, class _Float, class _Index>
class GraphSolver;

template<class _Solver>
class GraphSampler : public SamplerMixin<_Solver> {
public:
    typedef SamplerMixin<_Solver> Base;
    USING_SAMPLER_MIXIN(Base);
    using Base::Base;

    typedef GraphSolver<Solver::dim, Float, Index> GraphSolver;

    inline Attributes read_attributes(const Edge &edge) const override {
        return Attributes();
    }

    void sample_random_walk(int start, int end) {
        GraphSolver *solver = reinterpret_cast<GraphSolver *>(this->solver);

        CHECK(pool_size % solver->shuffle_base == 0)
                << "Can't perform matrix transpose on " << pool_size << " elements by "
                << solver->shuffle_base << " columns";
        CUDA_CHECK(cudaSetDevice(device_id));

        random.to_host();
        CURAND_CHECK(curandGenerateUniformDouble(generator, random.device_ptr, kRandBatchSize));

        auto &sample_pool = solver->sample_pools[solver->pool_id ^ 1];
        std::vector<std::vector<int>> offsets(solver->num_partition);
        for (auto &&partition_offsets : offsets)
            partition_offsets.resize(solver->num_partition, start);
        std::vector<std::vector<std::pair<int, Index>>> head_chains(solver->random_walk_batch_size);
        std::vector<std::vector<std::pair<int, Index>>> tail_chains(solver->random_walk_batch_size);
        for (auto &&head_chain : head_chains)
            head_chain.resize(solver->random_walk_length + 1);
        for (auto &&tail_chain : tail_chains)
            tail_chain.resize(solver->random_walk_length + 1);
        std::vector<int> sample_lengths(solver->random_walk_batch_size);
        int num_complete = 0, rand_id = 0;
        while (num_complete < solver->num_partition * solver->num_partition) {
            for (int i = 0; i < solver->random_walk_batch_size; i++) {
                if (rand_id > kRandBatchSize - (solver->random_walk_length + 1) * 2) {
                    random.to_host();
                    CURAND_CHECK(curandGenerateUniformDouble(generator, random.device_ptr, kRandBatchSize));
                    rand_id = 0;
                }
                Index index = random[rand_id++] * solver->degree_table.count;
                Float prob = random[rand_id++];

                Index current = solver->degree_table.sample(index, prob);
                head_chains[i][0] = solver->head_locations[current];
                tail_chains[i][0] = solver->tail_locations[current];
                sample_lengths[i] = solver->random_walk_length;
                for (int j = 1; j <= solver->random_walk_length; j++)
                    if (!solver->graph->vertex_edges[current].empty()) {
                        Index index = random[rand_id++] * solver->child_tables[current].count;
                        Float prob = random[rand_id++];
                        Index child_id = solver->child_tables[current].sample(index, prob);
                        current = std::get<0>(solver->graph->vertex_edges[current][child_id]);
                        head_chains[i][j] = solver->head_locations[current];
                        tail_chains[i][j] = solver->tail_locations[current];
                    } else {
                        sample_lengths[i] = j - 1;
                        break;
                    }
            }
            for (int i = 0; i < solver->random_walk_batch_size; i++) {
                for (int j = 0; j < sample_lengths[i]; j++) {
                    for (int k = 1; k <= solver->augmentation_step; k++) {
                        if (j + k > sample_lengths[i])
                            break;
                        // TODO: reject some random walk samples
                        int head_partition_id = head_chains[i][j].first;
                        int tail_partition_id = tail_chains[i][j + k].first;
                        int &offset = offsets[head_partition_id][tail_partition_id];
                        if (offset < end) {
                            auto &pool = sample_pool[head_partition_id][tail_partition_id];
                            Index head_local_id = head_chains[i][j].second;
                            Index tail_local_id = tail_chains[i][j + k].second;
                            // pseudo shuffle
                            int shuffled_offset = offset % solver->shuffle_base * (pool_size / solver->shuffle_base)
                                                  + offset / solver->shuffle_base;
                            pool[shuffled_offset] = std::make_tuple(head_local_id, tail_local_id);
                            if (++offset == end)
                                num_complete++;
                        }
                    }
                }
            }
        }
    }
};

template<class _Solver>
class GraphWorker : public WorkerMixin<_Solver> {
public:
    typedef WorkerMixin<_Solver> Base;
    USING_WORKER_MIXIN(Base);
    using Base::Base;

    typedef GraphSolver<Solver::dim, Float, Index> GraphSolver;

    Protocol get_sampler_protocol() const override {
        return kTailPartition;
    }

    bool kernel_dispatch() override {
        using namespace gpu::graph;
        GraphSolver *solver = reinterpret_cast<GraphSolver *>(this->solver);

        // model & optimizer distributed at runtime
        switch (num_moment) {
            case 0: {
                decltype(&line::train<Vector, Index, kSGD>) train = nullptr;
                if (solver->model == "LINE")
                    if (optimizer.type == "SGD")
                        train = &line::train<Vector, Index, kSGD>;
                if (train) {
                    train<<<gpu::kBlockPerGrid, gpu::kThreadPerBlock, 0, work_stream>>>
                            (*embeddings[0], *embeddings[1], batch, negative_batch, optimizer, solver->negative_weight
#ifdef USE_LOSS
                            , this->loss
#endif
                    );
                    return true;
                }
            }
        }
        return false;
    }
};

template <size_t _dim, class _Float = float, class _Index = size_t>
class GraphSolver :
        public SolverMixin<_dim, _Float, _Index, Graph, GraphSampler, GraphWorker> {
public:
    typedef SolverMixin<_dim, _Float, _Index, Graph, GraphSampler, GraphWorker> Base;
    USING_SOLVER_MIXIN(Base);
    using Base::Base;

    int augmentation_step, random_walk_length, random_walk_batch_size, shuffle_base;
    std::shared_ptr<std::vector<Vector>> vertex_embeddings, context_embeddings;

    AliasTable<Float, Index> degree_table = -1;
    std::vector<AliasTable<Float, Index>> child_tables;

    std::vector<Protocol> get_protocols() const override {
        return {kHeadPartition | kInPlace, kTailPartition | kInPlace};
    };

    std::vector<Index> get_shapes() const override {
        return {kAuto, kAuto};
    }

    std::set<std::string> get_available_models() const override {
        return {"DeepWalk", "LINE"};
    }

    Optimizer get_default_optimizer() const override {
        return SGD(0.025, 5e-3);
    }

    void build_alias() override {
        vertex_embeddings = embeddings[0];
        context_embeddings = embeddings[1];
    }

    std::function<void(Sampler*, int, int)> get_sample_function() override {
        if (augmentation_step == 1)
            return Base::get_sample_function();

        degree_table.build(graph->vertex_weights);
        for (auto &&vertex_edge : graph->vertex_edges) {
            std::vector<Float> child_weights;
            for (auto &&edge : vertex_edge)
                child_weights.push_back(std::get<1>(edge));
            AliasTable<Float, Index> child_table = -1;
            if (!child_weights.empty())
                child_table.build(child_weights);
            child_tables.push_back(child_table);
        }
        return &Base::Sampler::sample_random_walk;
    }

    void init_embeddings() override {
        std::uniform_real_distribution<Float> init(-0.5 / dim, 0.5 / dim);
        for (auto &&embedding : *vertex_embeddings)
            for (int i = 0; i < dim; i++)
                embedding[i] = init(seed);
        for (auto &&embedding : *context_embeddings)
            embedding = 0;
    }

    inline void print_name() const override {
        LOG(INFO) << "GraphSolver<" << dim << ", "
                  << pretty::type_name<Float>() << ", " << pretty::type_name<Index>() << ">";
    }

    inline void print_sampling_info() const override {
        LOG(INFO) << "augmentation step: " << augmentation_step << ", shuffle base: " << shuffle_base;
        LOG(INFO) << "random walk length: " << random_walk_length
                  << ", random walk batch size: " << random_walk_batch_size;
        LOG(INFO) << "#negative: " << num_negative << ", negative sample exponent: " << negative_sample_exponent;
    }

    void train(std::string _model = "LINE", int _num_epoch = 100, int _augmentation_step = 5,
               int _random_walk_length = 40, int _random_walk_batch_size = 100, int _shuffle_base = kAuto,
               int _positive_reuse = 1, float _negative_sample_exponent = 0.75, float _negative_weight = 5,
               int _log_frequency = 1000) {
        augmentation_step = _augmentation_step;
        random_walk_length = _random_walk_length;
        random_walk_batch_size = _random_walk_batch_size;
        shuffle_base = _shuffle_base;

        if (shuffle_base == kAuto)
            shuffle_base = augmentation_step;
        CHECK(augmentation_step >= 1) << "`augmentation_step` should be a positive integer";
        CHECK(augmentation_step <= random_walk_length)
                << "`random_walk_length` should be no less than `augmentation_step`";

        Base::train(_model, _num_epoch, random_walk_length * random_walk_batch_size, _positive_reuse,
                    _negative_sample_exponent, _negative_weight, _log_frequency);
    }

    void save_embeddings(const char *file_name) const {
        FILE *fout = fopen(file_name, "w");
        fprintf(fout, "%llu %llu\n", static_cast<unsigned long long>(num_vertex), static_cast<unsigned long long>(dim));
        for (Index i = 0; i < num_vertex; i++) {
            fprintf(fout, "%s ", graph->id2name[i].c_str());
            fwrite((*vertex_embeddings)[i].data, sizeof(Float), dim, fout);
            fprintf(fout, "\n");
        }
        fclose(fout);
    }
};

// ------------------- Knowledge Graph -------------------

template <size_t _dim, class _Float, class _Index>
class KnowledgeGraphSolver;

template<class _Solver>
#define _Index typename _Solver::Index
class KnowledgeGraphSampler : public SamplerMixin<_Solver, _Index> {
public:
    typedef SamplerMixin<_Solver, _Index> Base;
#undef _Index
    USING_SAMPLER_MIXIN(Base);
    using Base::Base;

    inline Attributes read_attributes(const Edge &edge) const override {
        return Attributes(std::get<2>(edge));
    }
};

template<class _Solver>
class KnowledgeGraphWorker : public WorkerMixin<_Solver> {
public:
    typedef WorkerMixin<_Solver> Base;
    USING_WORKER_MIXIN(Base);
    using Base::Base;

    typedef KnowledgeGraphSolver<Solver::dim, Float, Index> KnowledgeGraphSolver;

    Protocol get_sampler_protocol() const override {
        return kHeadPartition | kTailPartition;
    }

    void build_negative_sampler() override {
        std::vector<Float> negative_weights(head_partition_size + tail_partition_size, 1);
        negative_sampler.build(negative_weights);
    }

    bool kernel_dispatch() override {
        using namespace gpu::knowledge_graph;
        KnowledgeGraphSolver *solver = reinterpret_cast<KnowledgeGraphSolver *>(this->solver);

        // model & optimizer distributed at runtime
        switch (num_moment) {
            case 0: {
                decltype(&transe::train<Vector, Index, kSGD>) train = nullptr;
                if (solver->model == "TransE")
                    train = &transe::train<Vector, Index, kSGD>;
                if (solver->model == "RotatE")
                    train = &rotate::train<Vector, Index, kSGD>;
                if (train) {
                    train<<<gpu::kBlockPerGrid, gpu::kThreadPerBlock, 0, work_stream>>>
                            (*embeddings[0], *embeddings[1], *embeddings[2], *gradients[2],
                                    batch, negative_batch, optimizer, solver->margin, solver->adversarial_temperature
#ifdef USE_LOSS
                                    , this->loss
#endif
                    );
                    return true;
                }
                break;
            }
            case 2: {
                decltype(&transe::train_2_moment<Vector, Index, kAdam>) train = nullptr;
                if (solver->model == "TransE")
                    train = &transe::train_2_moment<Vector, Index, kAdam>;
                if (solver->model == "RotatE")
                    train = &rotate::train_2_moment<Vector, Index, kAdam>;
                if (solver->model == "DistMult")
                    train = &distmult::train_2_moment<Vector, Index, kAdam>;
                if (solver->model == "ComplEx")
                    train = &complex::train_2_moment<Vector, Index, kAdam>;
                if (train) {
                    train<<<gpu::kBlockPerGrid, gpu::kThreadPerBlock, 0, work_stream>>>
                            (*embeddings[0], *embeddings[1], *embeddings[2], *gradients[2],
                                    (*moments[0])[0], (*moments[1])[0], (*moments[2])[0],
                                    (*moments[0])[1], (*moments[1])[1], (*moments[2])[1],
                                    batch, negative_batch, optimizer, solver->margin, solver->adversarial_temperature
#ifdef USE_LOSS
                                    , this->loss
#endif
                    );
                    return true;
                }
                break;
            }
        }
        return false;
    }
};

template <size_t _dim, class _Float = float, class _Index = size_t>
class KnowledgeGraphSolver :
        public SolverMixin<_dim, _Float, _Index, KnowledgeGraph, KnowledgeGraphSampler, KnowledgeGraphWorker> {
public:
    typedef SolverMixin<_dim, _Float, _Index, KnowledgeGraph, KnowledgeGraphSampler, KnowledgeGraphWorker> Base;
    USING_SOLVER_MIXIN(Base);
    using Base::Base;

    float margin, adversarial_temperature;
    std::shared_ptr<std::vector<Vector>> entity_embeddings, relation_embeddings;

    std::vector<Protocol> get_protocols() const override {
        return {kHeadPartition | kInPlace, kTailPartition | kInPlace | kSharedWithPrevious, kGlobal};
    }

    std::vector<Index> get_shapes() const override {
        return {kAuto, kAuto, graph->num_relation};
    }

    std::set<std::string> get_available_models() const override {
        return {"TransE", "RotatE", "DistMult", "ComplEx"};
    }

    Optimizer get_default_optimizer() const override {
        return Adam(1e-4, 0);
    }

    void init_embeddings() override {
        static const Float kPi = atan(1) * 4;

        if (model == "TransE") {
            std::uniform_real_distribution<Float> init(-margin / dim, margin / dim);
            for (auto &&embedding : *entity_embeddings)
                for (int i = 0; i < dim; i++)
                    embedding[i] = init(seed);
            for (auto &&embedding : *relation_embeddings)
                for (int i = 0; i < dim; i++)
                    embedding[i] = init(seed);
        }
        if (model == "RotatE") {
            std::uniform_real_distribution<Float> init(-margin * 2 / dim, margin * 2 / dim);
            std::uniform_real_distribution<Float> init_phase(-kPi, kPi);
            for (auto &&embedding : *entity_embeddings)
                for (int i = 0; i < dim; i++)
                    embedding[i] = init(seed);
            for (auto &&embedding : *relation_embeddings)
                for (int i = 0; i < dim / 2; i++)
                    embedding[i] = init_phase(seed);
        }
        if (model == "DistMult" || model == "ComplEx") {
            std::uniform_real_distribution<Float> init(-0.5, 0.5);
            for (auto &&embedding : *entity_embeddings)
                for (int i = 0; i < dim; i++)
                    embedding[i] = init(seed);
            for (auto &&embedding : *relation_embeddings)
                for (int i = 0; i < dim; i++)
                    embedding[i] = init(seed);
        }
    }

    inline void print_name() const override {
        LOG(INFO) << "KnowledgeGraphSolver<" << dim << ", "
                  << pretty::type_name<Float>() << ", " << pretty::type_name<Index>() << ">";
    }

    virtual inline void print_sampling_info() const override {
        LOG(INFO) << "positive sample batch size: " << sample_batch_size;
        LOG(INFO) << "#negative: " << num_negative;
    }

    inline void print_training_info() const override {
        LOG(INFO) << "model: " << model;
        optimizer.print_info();
        LOG(INFO) << "#epoch: " << num_epoch << ", batch size: " << batch_size;
        LOG(INFO) << "margin: " << margin << ", positive reuse: " << positive_reuse;
        LOG(INFO) << "adversarial temperature: " << adversarial_temperature;
    }

    void build_alias() override {
        entity_embeddings = embeddings[0];
        relation_embeddings = embeddings[2];
    }

    void train(std::string _model = "RotatE", int _num_epoch = 100, float _margin = 24, int _sample_batch_size = 2000,
               int _positive_reuse = 1, float _adversarial_temperature = 1, int _log_frequency = 1000) {
        margin = _margin;
        adversarial_temperature = _adversarial_temperature;
        Base::train(_model, _num_epoch, _sample_batch_size, _positive_reuse, 0, 1.0f / num_negative, _log_frequency);
    }
};

// ------------------- Visualization -------------------

template <size_t _dim, class _Float, class _Index>
class VisualizationSolver;

template <class _Solver>
class VisualizationSampler : public SamplerMixin<_Solver> {
public:
    typedef SamplerMixin<_Solver> Base;
    USING_SAMPLER_MIXIN(Base);
    using Base::Base;

    inline Attributes read_attributes(const Edge &edge) const override {
        return Attributes();
    }
};

template <class _Solver>
class VisualizationWorker : public WorkerMixin<_Solver> {
    typedef WorkerMixin<_Solver> Base;
    USING_WORKER_MIXIN(Base);
    using Base::Base;

    typedef VisualizationSolver<Solver::dim, Float, Index> VisualizationSolver;

    Protocol get_sampler_protocol() const override {
        return kTailPartition;
    }

    bool kernel_dispatch() override {
        using namespace gpu::visualization;
        VisualizationSolver *solver = reinterpret_cast<VisualizationSolver *>(this->solver);

        // model & optimizer distributed at runtime
        switch (num_moment) {
            case 0: {
                decltype(&largevis::train<Vector, Index, kSGD>) train = nullptr;
                if (solver->model == "LargeVis")
                    train = &largevis::train<Vector, Index, kSGD>;
                if (train) {
                    train<<<gpu::kBlockPerGrid, gpu::kThreadPerBlock, 0, work_stream>>>
                            (*embeddings[0], *embeddings[1], batch, negative_batch, optimizer, solver->negative_weight
#ifdef USE_LOSS
                            , this->loss
#endif
                    );
                    return true;
                }
                break;
            }
            case 1: {
                decltype(&largevis::train_1_moment<Vector, Index, kMomentum>) train = nullptr;
                if (solver->model == "LargeVis")
                    train = &largevis::train_1_moment<Vector, Index, kMomentum>;
                if (train) {
                    train<<<gpu::kBlockPerGrid, gpu::kThreadPerBlock, 0, work_stream>>>
                            (*embeddings[0], *embeddings[1], (*moments[0])[0], (*moments[1])[0],
                                    batch, negative_batch, optimizer, solver->negative_weight
#ifdef USE_LOSS
                            , this->loss
#endif
                    );
                    return true;
                }
                break;
            }
            case 2: {
                decltype(&largevis::train_2_moment<Vector, Index, kAdam>) train = nullptr;
                if (solver->model == "LargeVis")
                    train = &largevis::train_2_moment<Vector, Index, kAdam>;
                if (train) {
                    train<<<gpu::kBlockPerGrid, gpu::kThreadPerBlock, 0, work_stream>>>
                            (*embeddings[0], *embeddings[1],
                                    (*moments[0])[0], (*moments[1])[0], (*moments[0])[1], (*moments[1])[1],
                                    batch, negative_batch, optimizer, solver->negative_weight
#ifdef USE_LOSS
                            , this->loss
#endif
                    );
                    return true;
                }
                break;
            }
        }
        return false;
    }
};

template <size_t _dim, class _Float = float, class _Index = size_t>
class VisualizationSolver :
        public SolverMixin<_dim, _Float, _Index, KNNGraph, VisualizationSampler, VisualizationWorker> {
public:
    typedef SolverMixin<_dim, _Float, _Index, KNNGraph, VisualizationSampler, VisualizationWorker> Base;
    USING_SOLVER_MIXIN(Base);
    using Base::Base;

    std::shared_ptr<std::vector<Vector>> coordinates;

    std::vector<Protocol> get_protocols() const override {
        return {kHeadPartition | kInPlace, kTailPartition | kInPlace | kSharedWithPrevious};
    };

    std::vector<Index> get_shapes() const override {
        return {kAuto, kAuto};
    }

    std::set<std::string> get_available_models() const override {
        return {"LargeVis"};
    }

    Optimizer get_default_optimizer() const override {
        return SGD(1.0, 0);
    }

    void build_alias() override {
        coordinates = embeddings[0];
    }

    void init_embeddings() override {
        std::uniform_real_distribution<Float> init(-5e-5 / dim, 5e-5 / dim);
        for (auto &&coordinate : *coordinates)
            for (int i = 0; i < dim; i++)
                coordinate[i] = init(seed);
    }

    inline void print_name() const override {
        LOG(INFO) << "VisualizationSolver<" << dim << ", "
                  << pretty::type_name<Float>() << ", " << pretty::type_name<Index>() << ">";
    }

    void train(std::string _model = "LargeVis", int _num_epoch = 100, int _sample_batch_size = 2000,
               int _positive_reuse = 1, float _negative_sample_exponent = 0.75, float _negative_weight = 35,
               int _log_frequency = 1000) {
        Base::train(_model, _num_epoch, _sample_batch_size, _positive_reuse,
                    _negative_sample_exponent, _negative_weight, _log_frequency);
    }
};

#undef USING_WORKER_MIXIN
#undef USING_SAMPLER_MIXIN
#undef USING_SOLVER_MIXIN

}; // namespace graphvite