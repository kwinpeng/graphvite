#pragma once

#include "memory.h"
#include "optimizer.h"
#include "util/gpu.cuh"

// Optimizers are binded at compile-time in GPU codes
// to maximize the speed of GPU
namespace graphvite {
namespace gpu {

namespace graph {

namespace line {
template<class Vector, class Index, OptimizerType type>
__global__ void train(Memory <Vector, Index> vertex_embeddings, Memory <Vector, Index> context_embeddings,
                      Memory<Index, int> batch, Memory<Index, int> negative_batch, Optimizer optimizer,
                      float negative_weight
#ifdef USE_LOSS
        , Memory<typename Vector::Float, int> loss
#endif
) {
    static const size_t dim = Vector::dim;
    typedef typename Vector::Float Float;

    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int lane_id = thread_id % kWarpSize;
    int num_thread = gridDim.x * blockDim.x;
    int batch_size = batch.count / 2;
    int num_negative = negative_batch.count / batch_size;

    auto update = get_update_function<Float, type>();

    __shared__ Vector buffer[kThreadPerBlock / kWarpSize];
    Vector &vertex_buffer = buffer[threadIdx.x / kWarpSize];

    for (int sample_id = thread_id / kWarpSize; sample_id < batch_size; sample_id += num_thread / kWarpSize) {
        // elements in std::tuple are stored in reverse order
        // each positive sample is {tail, head}
        Index head_id = batch[sample_id * 2 + 1];
        Vector &vertex = vertex_embeddings[head_id];
        for (int i = lane_id; i < dim; i += kWarpSize)
            vertex_buffer[i] = vertex[i];
#ifdef USE_LOSS
        Float sample_loss = 0;
#endif
        for (int s = 0; s <= num_negative; s++) {
            Index tail_id;
            int label;
            if (s < num_negative) {
                tail_id = negative_batch[sample_id * num_negative + s];
                label = 0;
            } else {
                tail_id = batch[sample_id * 2];
                label = 1;
            }
            Vector &context = context_embeddings[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize)
                x += vertex_buffer[i] * context[i];
            x = WarpBroadcast(WarpReduce(x), 0);
            Float prob = x > 0 ? 1 / (1 + exp(-x)) : exp(x) / (exp(x) + 1);
            // Backward
            Float gradient, weight;
            if (label) {
                gradient = prob - 1;
                weight = 1;
#ifdef USE_LOSS
                sample_loss += weight * -log(prob + kEpsilon);
#endif
            } else {
                gradient = prob;
                weight = negative_weight;
#ifdef USE_LOSS
                sample_loss += weight * -log(1 - prob + kEpsilon);
#endif
            }
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float v = vertex_buffer[i];
                Float c = context[i];
                context[i] -= (optimizer.*update)(c, gradient * v, weight);
                vertex_buffer[i] -= (optimizer.*update)(v, gradient * c, weight);
            }
        }
#ifdef USE_LOSS
        if (lane_id == 0)
            loss[sample_id] = sample_loss / (1 + num_negative * negative_weight);
#endif
        for (int i = lane_id; i < dim; i += kWarpSize)
            vertex[i] = vertex_buffer[i];
    }
}
} // namespace line
} // namespace graph

namespace knowledge_graph {

const float kLogitClip = 80;

namespace transe {

template<class Vector, class Index, OptimizerType type>
__global__ void train(Memory<Vector, Index> head_embeddings, Memory<Vector, Index> tail_embeddings,
                      Memory<Vector, Index> relation_embeddings, Memory<Vector, Index> relation_gradients,
                      Memory<Index, int> batch, Memory<Index, int> negative_batch,
                      Optimizer optimizer, float margin, float adversarial_temperature
#ifdef USE_LOSS
        , Memory<typename Vector::Float, int> loss
#endif
) {
    static const size_t dim = Vector::dim;
    typedef typename Vector::Float Float;

    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int lane_id = thread_id % kWarpSize;
    int num_thread = gridDim.x * blockDim.x;
    int num_head = head_embeddings.count;
    int batch_size = batch.count / 3;
    int num_negative = negative_batch.count / batch_size;

    auto update = get_update_function<Float, type>();

    __shared__ graphvite::Vector<dim, bool> buffer[kThreadPerBlock / kWarpSize];

    graphvite::Vector<dim, bool> &sign = buffer[threadIdx.x / kWarpSize];

    for (int sample_id = thread_id / kWarpSize; sample_id < batch_size; sample_id += num_thread / kWarpSize) {
        // elements in std::tuple are stored in reverse order
        // each positive sample is {relation, tail, head}
        Index relation_id = batch[sample_id * 3];
        Vector &relation = relation_embeddings[relation_id];
        Vector &relation_gradient = relation_gradients[relation_id];

        // compute normalizer
        Float x0, normalizer = 0;
        for (int s = 0; s < num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            Index negative_id = negative_batch[sample_id * num_negative + s];
            if (negative_id < num_head)
                head_id = negative_id;
            else
                tail_id = negative_id - num_head;
            Vector &head = head_embeddings[head_id];
            Vector &tail = tail_embeddings[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize)
                x += abs(head[i] + relation[i] - tail[i]);
            x = WarpBroadcast(WarpReduce(x), 0);
            x = margin - x;
            if (s == 0)
                x0 = x;
            normalizer += exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip));
        }

#ifdef USE_LOSS
        Float sample_loss = 0;
#endif
        for (int s = 0; s <= num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            int label = 1;
            if (s < num_negative) {
                Index negative_id = negative_batch[sample_id * num_negative + s];
                if (negative_id < num_head)
                    head_id = negative_id;
                else
                    tail_id = negative_id - num_head;
                label = 0;
            }
            Vector &head = head_embeddings[head_id];
            Vector &tail = tail_embeddings[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float y = head[i] + relation[i] - tail[i];
                sign[i] = y > 0;
                x += abs(y);
            }
            x = WarpBroadcast(WarpReduce(x), 0);
            x = margin - x;
            Float prob = x > 0 ? 1 / (1 + exp(-x)) : exp(x) / (exp(x) + 1);
            // Backward
            Float gradient, weight;
            if (label) {
                gradient = prob - 1;
                weight = 1;
#ifdef USE_LOSS
                sample_loss += weight * -log(prob + kEpsilon);
#endif
            } else {
                gradient = prob;
                weight = exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip)) / normalizer;
#ifdef USE_LOSS
                sample_loss += weight * -log(1 - prob + kEpsilon);
#endif
            }
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float s = sign[i] ? 1 : -1;
                head[i] -= (optimizer.*update)(head[i], -gradient * s, weight);
                tail[i] -= (optimizer.*update)(tail[i], gradient * s, weight);
                Float relation_update = (optimizer.*update)(relation[i], -gradient * s, weight);
                relation[i] -= relation_update;
                relation_gradient[i] += relation_update;
            }
#ifdef USE_LOSS
            if (lane_id == 0)
                loss[sample_id] = sample_loss / 2;
#endif
        }
    }
}

template<class Vector, class Index, OptimizerType type>
__global__ void train_2_moment(Memory<Vector, Index> head_embeddings, Memory<Vector, Index> tail_embeddings,
                               Memory<Vector, Index> relation_embeddings, Memory<Vector, Index> relation_gradients,
                               Memory<Vector, Index> head_moment1s, Memory<Vector, Index> tail_moment1s,
                               Memory<Vector, Index> relation_moment1s, Memory<Vector, Index> head_moment2s,
                               Memory<Vector, Index> tail_moment2s, Memory<Vector, Index> relation_moment2s,
                               Memory<Index, int> batch, Memory<Index, int> negative_batch,
                               Optimizer optimizer, float margin, float adversarial_temperature
#ifdef USE_LOSS
        , Memory<typename Vector::Float, int> loss
#endif
) {
    static const size_t dim = Vector::dim;
    typedef typename Vector::Float Float;

    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int lane_id = thread_id % kWarpSize;
    int num_thread = gridDim.x * blockDim.x;
    int num_head = head_embeddings.count;
    int batch_size = batch.count / 3;
    int num_negative = negative_batch.count / batch_size;

    auto update = get_update_function_2_moment<Float, type>();

    __shared__ graphvite::Vector<dim, bool> buffer[kThreadPerBlock / kWarpSize];

    graphvite::Vector<dim, bool> &sign = buffer[threadIdx.x / kWarpSize];

    for (int sample_id = thread_id / kWarpSize; sample_id < batch_size; sample_id += num_thread / kWarpSize) {
        // elements in std::tuple are stored in reverse order
        // each positive sample is {relation, tail, head}
        Index relation_id = batch[sample_id * 3];
        Vector &relation = relation_embeddings[relation_id];
        Vector &relation_moment1 = relation_moment1s[relation_id];
        Vector &relation_moment2 = relation_moment2s[relation_id];
        Vector &relation_gradient = relation_gradients[relation_id];

        // compute normalizer
        Float x0, normalizer = 0;
        for (int s = 0; s < num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            Index negative_id = negative_batch[sample_id * num_negative + s];
            if (negative_id < num_head)
                head_id = negative_id;
            else
                tail_id = negative_id - num_head;
            Vector &head = head_embeddings[head_id];
            Vector &tail = tail_embeddings[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize)
                x += abs(head[i] + relation[i] - tail[i]);
            x = WarpBroadcast(WarpReduce(x), 0);
            x = margin - x;
            if (s == 0)
                x0 = x;
            normalizer += exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip));
        }

#ifdef USE_LOSS
        Float sample_loss = 0;
#endif
        for (int s = 0; s <= num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            int label = 1;
            if (s < num_negative) {
                Index negative_id = negative_batch[sample_id * num_negative + s];
                if (negative_id < num_head)
                    head_id = negative_id;
                else
                    tail_id = negative_id - num_head;
                label = 0;
            }
            Vector &head = head_embeddings[head_id];
            Vector &head_moment1 = head_moment1s[head_id];
            Vector &head_moment2 = head_moment2s[head_id];
            Vector &tail = tail_embeddings[tail_id];
            Vector &tail_moment1 = tail_moment1s[tail_id];
            Vector &tail_moment2 = tail_moment2s[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float y = head[i] + relation[i] - tail[i];
                sign[i] = y > 0;
                x += abs(y);
            }
            x = WarpBroadcast(WarpReduce(x), 0);
            x = margin - x;
            Float prob = x > 0 ? 1 / (1 + exp(-x)) : exp(x) / (exp(x) + 1);
            // Backward
            Float gradient, weight;
            if (label) {
                gradient = prob - 1;
                weight = 1;
#ifdef USE_LOSS
                sample_loss += weight * -log(prob + kEpsilon);
#endif
            } else {
                gradient = prob;
                weight = exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip)) / normalizer;
#ifdef USE_LOSS
                sample_loss += weight * -log(1 - prob + kEpsilon);
#endif
            }
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float s = sign[i] ? 1 : -1;
                head[i] -= (optimizer.*update)(head[i], -gradient * s, head_moment1[i], head_moment2[i], weight);
                tail[i] -= (optimizer.*update)(tail[i], gradient * s, tail_moment1[i], tail_moment2[i], weight);
                Float relation_update = (optimizer.*update)(relation[i], -gradient * s,
                                                            relation_moment1[i], relation_moment2[i], weight);
                relation[i] -= relation_update;
                relation_gradient[i] += relation_update;
            }
#ifdef USE_LOSS
            if (lane_id == 0)
                loss[sample_id] = sample_loss / 2;
#endif
        }
    }
}
} // namespace transe

namespace rotate {

template<class Vector, class Index, OptimizerType type>
__global__ void train(Memory<Vector, Index> head_embeddings, Memory<Vector, Index> tail_embeddings,
                      Memory<Vector, Index> relation_embeddings, Memory <Vector, Index> relation_gradients,
                      Memory<Index, int> batch, Memory<Index, int> negative_batch,
                      Optimizer optimizer, float margin, float adversarial_temperature
#ifdef USE_LOSS
        , Memory<typename Vector::Float, int> loss
#endif
) {
    static const size_t dim = Vector::dim / 2;
    typedef typename Vector::Float Float;

    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int lane_id = thread_id % kWarpSize;
    int num_thread = gridDim.x * blockDim.x;
    int num_head = head_embeddings.count;
    int batch_size = batch.count / 3;
    int num_negative = negative_batch.count / batch_size;

    auto update = get_update_function<Float, type>();

//    __shared__ graphvite::Vector<dim, bool> buffer[kThreadPerBlock / kWarpSize];

//    graphvite::Vector<dim, bool> &sign = buffer[threadIdx.x / kWarpSize];

    for (int sample_id = thread_id / kWarpSize; sample_id < batch_size; sample_id += num_thread / kWarpSize) {
        // elements in std::tuple are stored in reverse order
        // each positive sample is {relation, tail, head}
        Index relation_id = batch[sample_id * 3];
        Vector &relation = relation_embeddings[relation_id];
        Vector &relation_gradient = relation_gradients[relation_id];

        // compute normalizer
        Float x0, normalizer = 0;
        for (int s = 0; s < num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            Index negative_id = negative_batch[sample_id * num_negative + s];
            if (negative_id < num_head)
                head_id = negative_id;
            else
                tail_id = negative_id - num_head;
            Vector &head = head_embeddings[head_id];
            Vector &tail = tail_embeddings[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float head_re = head[i * 2];
                Float head_im = head[i * 2 + 1];
                Float tail_re = tail[i * 2];
                Float tail_im = tail[i * 2 + 1];
                Float relation_phase = relation[i];
                Float relation_re = cos(relation_phase);
                Float relation_im = sin(relation_phase);
                Float distance_re = head_re * relation_re - head_im * relation_im - tail_re;
                Float distance_im = head_re * relation_im + head_im * relation_re - tail_im;
                x += sqrt(distance_re * distance_re + distance_im * distance_im);
            }
            x = WarpBroadcast(WarpReduce(x), 0);
            x = margin - x;
            if (s == 0)
                x0 = x;
            normalizer += exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip));
        }

#ifdef USE_LOSS
        Float sample_loss = 0;
#endif
        for (int s = 0; s <= num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            int label = 1;
            if (s < num_negative) {
                Index negative_id = negative_batch[sample_id * num_negative + s];
                if (negative_id < num_head)
                    head_id = negative_id;
                else
                    tail_id = negative_id - num_head;
                label = 0;
            }
            Vector &head = head_embeddings[head_id];
            Vector &tail = tail_embeddings[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float head_re = head[i * 2];
                Float head_im = head[i * 2 + 1];
                Float tail_re = tail[i * 2];
                Float tail_im = tail[i * 2 + 1];
                Float relation_phase = relation[i];
                Float relation_re = cos(relation_phase);
                Float relation_im = sin(relation_phase);
                Float distance_re = head_re * relation_re - head_im * relation_im - tail_re;
                Float distance_im = head_re * relation_im + head_im * relation_re - tail_im;
                x += sqrt(distance_re * distance_re + distance_im * distance_im);
            }
            x = WarpBroadcast(WarpReduce(x), 0);
            x = margin - x;
            Float prob = x > 0 ? 1 / (1 + exp(-x)) : exp(x) / (exp(x) + 1);
            // Backward
            Float gradient, weight;
            if (label) {
                gradient = prob - 1;
                weight = 1;
#ifdef USE_LOSS
                sample_loss += weight * -log(prob + kEpsilon);
#endif
            } else {
                gradient = prob;
                weight = exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip)) / normalizer;
#ifdef USE_LOSS
                sample_loss += weight * -log(1 - prob + kEpsilon);
#endif
            }
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float relation_phase = relation[i];
                Float relation_re = cos(relation_phase);
                Float relation_im = sin(relation_phase);
                Float head_re = head[i * 2];
                Float head_im = head[i * 2 + 1];
                Float tail_re = tail[i * 2];
                Float tail_im = tail[i * 2 + 1];
                Float distance_re = head_re * relation_re - head_im * relation_im - tail_re;
                Float distance_im = head_re * relation_im + head_im * relation_re - tail_im;
                Float grad_this_dim =
                        gradient / (sqrt(distance_re * distance_re + distance_im * distance_im) + kEpsilon);
                // head
                Float head_re_grad = -grad_this_dim * (distance_re * relation_re + distance_im * relation_im);
                Float head_im_grad = -grad_this_dim * (-distance_re * relation_im + distance_im * relation_re);
                head[i * 2] -= (optimizer.*update)(head_re, head_re_grad, weight);
                head[i * 2 + 1] -= (optimizer.*update)(head_im, head_im_grad, weight);
                // tail
                tail[i * 2] -= (optimizer.*update)(tail_re, grad_this_dim * distance_re, weight);
                tail[i * 2 + 1] -= (optimizer.*update)(tail_im, grad_this_dim * distance_im, weight);
                // relation
                Float relation_grad = -grad_this_dim *
                                      (distance_re * (head_re * -relation_im + head_im * -relation_re) +
                                       distance_im * (head_re * relation_re + head_im * -relation_im));
                Float relation_update = (optimizer.*update)(relation_phase, relation_grad, weight);
                relation[i] -= relation_update;
                relation_gradient[i] += relation_update;
            }
#ifdef USE_LOSS
            if (lane_id == 0)
                loss[sample_id] = sample_loss / 2;
#endif
        }
    }
}

template<class Vector, class Index, OptimizerType type>
__global__ void train_2_moment(Memory<Vector, Index> head_embeddings, Memory<Vector, Index> tail_embeddings,
                               Memory<Vector, Index> relation_embeddings, Memory<Vector, Index> relation_gradients,
                               Memory<Vector, Index> head_moment1s, Memory<Vector, Index> tail_moment1s,
                               Memory<Vector, Index> relation_moment1s, Memory<Vector, Index> head_moment2s,
                               Memory<Vector, Index> tail_moment2s, Memory<Vector, Index> relation_moment2s,
                               Memory<Index, int> batch, Memory<Index, int> negative_batch,
                               Optimizer optimizer, float margin, float adversarial_temperature
#ifdef USE_LOSS
        , Memory<typename Vector::Float, int> loss
#endif
) {
    static const size_t dim = Vector::dim / 2;
    typedef typename Vector::Float Float;

    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int lane_id = thread_id % kWarpSize;
    int num_thread = gridDim.x * blockDim.x;
    int num_head = head_embeddings.count;
    int batch_size = batch.count / 3;
    int num_negative = negative_batch.count / batch_size;

    auto update = get_update_function_2_moment<Float, type>();

//    __shared__ graphvite::Vector<dim, bool> buffer[kThreadPerBlock / kWarpSize];

//    graphvite::Vector<dim, bool> &sign = buffer[threadIdx.x / kWarpSize];

    for (int sample_id = thread_id / kWarpSize; sample_id < batch_size; sample_id += num_thread / kWarpSize) {
        // elements in std::tuple are stored in reverse order
        // each positive sample is {relation, tail, head}
        Index relation_id = batch[sample_id * 3];
        Vector &relation = relation_embeddings[relation_id];
        Vector &relation_moment1 = relation_moment1s[relation_id];
        Vector &relation_moment2 = relation_moment2s[relation_id];
        Vector &relation_gradient = relation_gradients[relation_id];

        // compute normalizer
        Float x0, normalizer = 0;
        for (int s = 0; s < num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            Index negative_id = negative_batch[sample_id * num_negative + s];
            if (negative_id < num_head)
                head_id = negative_id;
            else
                tail_id = negative_id - num_head;
            Vector &head = head_embeddings[head_id];
            Vector &tail = tail_embeddings[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float head_re = head[i * 2];
                Float head_im = head[i * 2 + 1];
                Float tail_re = tail[i * 2];
                Float tail_im = tail[i * 2 + 1];
                Float relation_phase = relation[i];
                Float relation_re = cos(relation_phase);
                Float relation_im = sin(relation_phase);
                Float distance_re = head_re * relation_re - head_im * relation_im - tail_re;
                Float distance_im = head_re * relation_im + head_im * relation_re - tail_im;
                x += sqrt(distance_re * distance_re + distance_im * distance_im);
            }
            x = WarpBroadcast(WarpReduce(x), 0);
            x = margin - x;
            if (s == 0)
                x0 = x;
            normalizer += exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip));
        }

#ifdef USE_LOSS
        Float sample_loss = 0;
#endif
        for (int s = 0; s <= num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            int label = 1;
            if (s < num_negative) {
                Index negative_id = negative_batch[sample_id * num_negative + s];
                if (negative_id < num_head)
                    head_id = negative_id;
                else
                    tail_id = negative_id - num_head;
                label = 0;
            }
            Vector &head = head_embeddings[head_id];
            Vector &head_moment1 = head_moment1s[head_id];
            Vector &head_moment2 = head_moment2s[head_id];
            Vector &tail = tail_embeddings[tail_id];
            Vector &tail_moment1 = tail_moment1s[tail_id];
            Vector &tail_moment2 = tail_moment2s[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float head_re = head[i * 2];
                Float head_im = head[i * 2 + 1];
                Float tail_re = tail[i * 2];
                Float tail_im = tail[i * 2 + 1];
                Float relation_phase = relation[i];
                Float relation_re = cos(relation_phase);
                Float relation_im = sin(relation_phase);
                Float distance_re = head_re * relation_re - head_im * relation_im - tail_re;
                Float distance_im = head_re * relation_im + head_im * relation_re - tail_im;
                x += sqrt(distance_re * distance_re + distance_im * distance_im);
            }
            x = WarpBroadcast(WarpReduce(x), 0);
            x = margin - x;
            Float prob = x > 0 ? 1 / (1 + exp(-x)) : exp(x) / (exp(x) + 1);
            // Backward
            Float gradient, weight;
            if (label) {
                gradient = prob - 1;
                weight = 1;
#ifdef USE_LOSS
                sample_loss += weight * -log(prob + kEpsilon);
#endif
            } else {
                gradient = prob;
                weight = exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip)) / normalizer;
#ifdef USE_LOSS
                sample_loss += weight * -log(1 - prob + kEpsilon);
#endif
            }
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float relation_phase = relation[i];
                Float relation_re = cos(relation_phase);
                Float relation_im = sin(relation_phase);
                Float head_re = head[i * 2];
                Float head_im = head[i * 2 + 1];
                Float tail_re = tail[i * 2];
                Float tail_im = tail[i * 2 + 1];
                Float distance_re = head_re * relation_re - head_im * relation_im - tail_re;
                Float distance_im = head_re * relation_im + head_im * relation_re - tail_im;
                Float grad_this_dim = gradient /
                        (sqrt(distance_re * distance_re + distance_im * distance_im) + kEpsilon);
                // head
                Float head_re_grad = -grad_this_dim * (distance_re * relation_re + distance_im * relation_im);
                Float head_im_grad = -grad_this_dim * (-distance_re * relation_im + distance_im * relation_re);
                head[i * 2] -= (optimizer.*update)(head_re, head_re_grad,
                                                   head_moment1[i * 2], head_moment2[i * 2], weight);
                head[i * 2 + 1] -= (optimizer.*update)(head_im, head_im_grad,
                                                       head_moment1[i * 2 + 1], head_moment2[i * 2 + 1], weight);
                // tail
                tail[i * 2] -= (optimizer.*update)(tail_re, grad_this_dim * distance_re,
                                                   tail_moment1[i * 2], tail_moment2[i * 2], weight);
                tail[i * 2 + 1] -= (optimizer.*update)(tail_im, grad_this_dim * distance_im,
                                                       tail_moment1[i * 2 + 1], tail_moment2[i * 2 + 1], weight);
                // relation
                Float relation_grad = -grad_this_dim *
                                      (distance_re * (head_re * -relation_im + head_im * -relation_re) +
                                       distance_im * (head_re * relation_re + head_im * -relation_im));
                Float relation_update = (optimizer.*update)(relation_phase, relation_grad,
                                                            relation_moment1[i], relation_moment2[i], weight);
                relation[i] -= relation_update;
                relation_gradient[i] += relation_update;
            }
#ifdef USE_LOSS
            if (lane_id == 0)
                loss[sample_id] = sample_loss / 2;
#endif
        }
    }
}
} // namespace rotate

namespace distmult {
template<class Vector, class Index, OptimizerType type>
__global__ void train_2_moment(Memory<Vector, Index> head_embeddings, Memory<Vector, Index> tail_embeddings,
                               Memory<Vector, Index> relation_embeddings, Memory<Vector, Index> relation_gradients,
                               Memory<Vector, Index> head_moment1s, Memory<Vector, Index> tail_moment1s,
                               Memory<Vector, Index> relation_moment1s, Memory<Vector, Index> head_moment2s,
                               Memory<Vector, Index> tail_moment2s, Memory<Vector, Index> relation_moment2s,
                               Memory<Index, int> batch, Memory<Index, int> negative_batch,
                               Optimizer optimizer, float margin, float adversarial_temperature
#ifdef USE_LOSS
        , Memory<typename Vector::Float, int> loss
#endif
) {
    static const size_t dim = Vector::dim;
    typedef typename Vector::Float Float;

    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int lane_id = thread_id % kWarpSize;
    int num_thread = gridDim.x * blockDim.x;
    int num_head = head_embeddings.count;
    int batch_size = batch.count / 3;
    int num_negative = negative_batch.count / batch_size;
    margin *= 3; // L3 regulation

    auto update = get_update_function_2_moment<Float, type>();

    for (int sample_id = thread_id / kWarpSize; sample_id < batch_size; sample_id += num_thread / kWarpSize) {
        // elements in std::tuple are stored in reverse order
        // each positive sample is {relation, tail, head}
        Index relation_id = batch[sample_id * 3];
        Vector &relation = relation_embeddings[relation_id];
        Vector &relation_moment1 = relation_moment1s[relation_id];
        Vector &relation_moment2 = relation_moment2s[relation_id];
        Vector &relation_gradient = relation_gradients[relation_id];

        // compute normalizer
        Float x0, normalizer = 0;
        for (int s = 0; s < num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            Index negative_id = negative_batch[sample_id * num_negative + s];
            if (negative_id < num_head)
                head_id = negative_id;
            else
                tail_id = negative_id - num_head;
            Vector &head = head_embeddings[head_id];
            Vector &tail = tail_embeddings[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize)
                x += head[i] * relation[i] * tail[i];
            x = WarpBroadcast(WarpReduce(x), 0);
            if (s == 0)
                x0 = x;
            normalizer += exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip));
        }

#ifdef USE_LOSS
        Float sample_loss = 0;
#endif
        for (int s = 0; s <= num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            int label = 1;
            if (s < num_negative) {
                Index negative_id = negative_batch[sample_id * num_negative + s];
                if (negative_id < num_head)
                    head_id = negative_id;
                else
                    tail_id = negative_id - num_head;
                label = 0;
            }
            Vector &head = head_embeddings[head_id];
            Vector &head_moment1 = head_moment1s[head_id];
            Vector &head_moment2 = head_moment2s[head_id];
            Vector &tail = tail_embeddings[tail_id];
            Vector &tail_moment1 = tail_moment1s[tail_id];
            Vector &tail_moment2 = tail_moment2s[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize)
                x += head[i] * relation[i] * tail[i];
            x = WarpBroadcast(WarpReduce(x), 0);
            Float prob = x > 0 ? 1 / (1 + exp(-x)) : exp(x) / (exp(x) + 1);
            // Backward
            Float gradient, weight;
            if (label) {
                gradient = prob - 1;
                weight = 1;
#ifdef USE_LOSS
                sample_loss += weight * -log(prob + kEpsilon);
#endif
            } else {
                gradient = prob;
                weight = exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip)) / normalizer;
#ifdef USE_LOSS
                sample_loss += weight * -log(1 - prob + kEpsilon);
#endif
            }
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float h = head[i];
                Float r = relation[i];
                Float t = tail[i];
                head[i] -= (optimizer.*update)(h, gradient * r * t + margin * abs(h) * h,
                                               head_moment1[i], head_moment2[i], weight);
                tail[i] -= (optimizer.*update)(t, gradient * h * r + margin * abs(t) * t,
                                               tail_moment1[i], tail_moment2[i], weight);
                Float relation_update = (optimizer.*update)(r, gradient * h * t + margin * abs(r) * r,
                                                            relation_moment1[i], relation_moment2[i], weight);
                relation[i] -= relation_update;
                relation_gradient[i] += relation_update;
            }
#ifdef USE_LOSS
            if (lane_id == 0)
                loss[sample_id] = sample_loss / 2;
#endif
        }
    }
}
} // namespace distmult

namespace complex {
template<class Vector, class Index, OptimizerType type>
__global__ void train_2_moment(Memory<Vector, Index> head_embeddings, Memory<Vector, Index> tail_embeddings,
                               Memory<Vector, Index> relation_embeddings, Memory<Vector, Index> relation_gradients,
                               Memory<Vector, Index> head_moment1s, Memory<Vector, Index> tail_moment1s,
                               Memory<Vector, Index> relation_moment1s, Memory<Vector, Index> head_moment2s,
                               Memory<Vector, Index> tail_moment2s, Memory<Vector, Index> relation_moment2s,
                               Memory<Index, int> batch, Memory<Index, int> negative_batch,
                               Optimizer optimizer, float margin, float adversarial_temperature
#ifdef USE_LOSS
        , Memory<typename Vector::Float, int> loss
#endif
) {
    static const size_t dim = Vector::dim / 2;
    typedef typename Vector::Float Float;

    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int lane_id = thread_id % kWarpSize;
    int num_thread = gridDim.x * blockDim.x;
    int num_head = head_embeddings.count;
    int batch_size = batch.count / 3;
    int num_negative = negative_batch.count / batch_size;
    margin *= 3; // L3 regulation

    auto update = get_update_function_2_moment<Float, type>();

    for (int sample_id = thread_id / kWarpSize; sample_id < batch_size; sample_id += num_thread / kWarpSize) {
        // elements in std::tuple are stored in reverse order
        // each positive sample is {relation, tail, head}
        Index relation_id = batch[sample_id * 3];
        Vector &relation = relation_embeddings[relation_id];
        Vector &relation_moment1 = relation_moment1s[relation_id];
        Vector &relation_moment2 = relation_moment2s[relation_id];
        Vector &relation_gradient = relation_gradients[relation_id];

        // compute normalizer
        Float x0, normalizer = 0;
        for (int s = 0; s < num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            Index negative_id = negative_batch[sample_id * num_negative + s];
            if (negative_id < num_head)
                head_id = negative_id;
            else
                tail_id = negative_id - num_head;
            Vector &head = head_embeddings[head_id];
            Vector &tail = tail_embeddings[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float head_re = head[i * 2];
                Float head_im = head[i * 2 + 1];
                Float tail_re = tail[i * 2];
                Float tail_im = tail[i * 2 + 1];
                Float relation_re = relation[i * 2];
                Float relation_im = relation[i * 2 + 1];
                Float product_re = head_re * relation_re - head_im * relation_im;
                Float product_im = head_re * relation_im + head_im * relation_re;
                x += product_re * tail_re + product_im * tail_im;
            }
            x = WarpBroadcast(WarpReduce(x), 0);
            if (s == 0)
                x0 = x;
            normalizer += exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip));
        }

#ifdef USE_LOSS
        Float sample_loss = 0;
#endif
        for (int s = 0; s <= num_negative; s++) {
            Index head_id = batch[sample_id * 3 + 2];
            Index tail_id = batch[sample_id * 3 + 1];
            int label = 1;
            if (s < num_negative) {
                Index negative_id = negative_batch[sample_id * num_negative + s];
                if (negative_id < num_head)
                    head_id = negative_id;
                else
                    tail_id = negative_id - num_head;
                label = 0;
            }
            Vector &head = head_embeddings[head_id];
            Vector &head_moment1 = head_moment1s[head_id];
            Vector &head_moment2 = head_moment2s[head_id];
            Vector &tail = tail_embeddings[tail_id];
            Vector &tail_moment1 = tail_moment1s[tail_id];
            Vector &tail_moment2 = tail_moment2s[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float head_re = head[i * 2];
                Float head_im = head[i * 2 + 1];
                Float tail_re = tail[i * 2];
                Float tail_im = tail[i * 2 + 1];
                Float relation_re = relation[i * 2];
                Float relation_im = relation[i * 2 + 1];
                Float product_re = head_re * relation_re - head_im * relation_im;
                Float product_im = head_re * relation_im + head_im * relation_re;
                x += product_re * tail_re + product_im * tail_im;
            }
            x = WarpBroadcast(WarpReduce(x), 0);
            Float prob = x > 0 ? 1 / (1 + exp(-x)) : exp(x) / (exp(x) + 1);
            // Backward
            Float gradient, weight;
            if (label) {
                gradient = prob - 1;
                weight = 1;
#ifdef USE_LOSS
                sample_loss += weight * -log(prob + kEpsilon);
#endif
            } else {
                gradient = prob;
                weight = exp(min(max((x - x0) * adversarial_temperature, -kLogitClip), kLogitClip)) / normalizer;
#ifdef USE_LOSS
                sample_loss += weight * -log(1 - prob + kEpsilon);
#endif
            }
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float head_re = head[i * 2];
                Float head_im = head[i * 2 + 1];
                Float tail_re = tail[i * 2];
                Float tail_im = tail[i * 2 + 1];
                Float relation_re = relation[i * 2];
                Float relation_im = relation[i * 2 + 1];
                // head
                Float head_re_grad = gradient * (relation_re * tail_re + relation_im * tail_im);
                Float head_im_grad = gradient * (-relation_im * tail_re + relation_re * tail_im);
                head[i * 2] -= (optimizer.*update)(head_re, head_re_grad + margin * abs(head_re) * head_re,
                                                   head_moment1[i * 2], head_moment2[i * 2], weight);
                head[i * 2 + 1] -= (optimizer.*update)(head_im, head_im_grad + margin * abs(head_im) * head_im,
                                                       head_moment1[i * 2 + 1], head_moment2[i * 2 + 1], weight);
                // tail
                Float tail_re_grad = gradient * (head_re * relation_re - head_im * relation_im);
                Float tail_im_grad = gradient * (head_re * relation_im + head_im * relation_re);
                tail[i * 2] -= (optimizer.*update)(tail_re, tail_re_grad + margin * abs(tail_re) * tail_re,
                                                   tail_moment1[i * 2], tail_moment2[i * 2], weight);
                tail[i * 2 + 1] -= (optimizer.*update)(tail_im, tail_im_grad + margin * abs(tail_im) * tail_im,
                                                       tail_moment1[i * 2 + 1], tail_moment2[i * 2 + 1], weight);
                // relation
                Float relation_re_grad = gradient * (head_re * tail_re + head_im * tail_im);
                Float relation_im_grad = gradient * (-head_im * tail_re + head_re * tail_im);
                Float relation_re_update = (optimizer.*update)
                        (relation_re, relation_re_grad + margin * abs(relation_re) * relation_re,
                         relation_moment1[i], relation_moment2[i], weight);
                Float relation_im_update = (optimizer.*update)
                        (relation_im, relation_im_grad + margin * abs(relation_im) * relation_im,
                         relation_moment1[i], relation_moment2[i], weight);
                relation[i * 2] -= relation_re_update;
                relation[i * 2 + 1] -= relation_im_update;
                relation_gradient[i * 2] += relation_re_update;
                relation_gradient[i * 2 + 1] += relation_im_update;
            }
#ifdef USE_LOSS
            if (lane_id == 0)
                loss[sample_id] = sample_loss / 2;
#endif
        }
    }
}
} // namespace complex
} // namespace knowledge graph

namespace visualization {
namespace largevis {

// necessary for smooth convergence
// otherwise we need to adopt many tricks like gradient clip
const float kSmoothTerm = 1;

template<class Vector, class Index, OptimizerType type>
__global__ void train(Memory<Vector, Index> head_embeddings, Memory<Vector, Index> tail_embeddings,
                      Memory<Index, int> batch, Memory<Index, int> negative_batch, Optimizer optimizer,
                      float negative_weight
#ifdef USE_LOSS
        , Memory<typename Vector::Float, int> loss
#endif
) {
    static const size_t dim = Vector::dim;
    typedef typename Vector::Float Float;

    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int lane_id = thread_id % kWarpSize;
    int num_thread = gridDim.x * blockDim.x;
    int batch_size = batch.count / 2;
    int num_negative = negative_batch.count / batch_size;

    auto update = get_update_function<Float, type>();

    __shared__ Vector buffer[kThreadPerBlock / kWarpSize];
    Vector &head_buffer = buffer[threadIdx.x / kWarpSize];

    for (int sample_id = thread_id / kWarpSize; sample_id < batch_size; sample_id += num_thread / kWarpSize) {
        // elements in std::tuple are stored in reverse order
        // each positive sample is {tail, head}
        Index head_id = batch[sample_id * 2 + 1];
        Vector &head = head_embeddings[head_id];
        for (int i = lane_id; i < dim; i += kWarpSize)
            head_buffer[i] = head[i];
#ifdef USE_LOSS
        Float sample_loss = 0;
#endif
        for (int s = 0; s <= num_negative; s++) {
            Index tail_id;
            int label;
            if (s < num_negative) {
                tail_id = negative_batch[sample_id * num_negative + s];
                label = 0;
            } else {
                tail_id = batch[sample_id * 2];
                label = 1;
            }
            Vector &tail = tail_embeddings[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize)
                x += (head_buffer[i] - tail[i]) * (head_buffer[i] - tail[i]);
            x = WarpBroadcast(WarpReduce(x), 0) + kSmoothTerm;
            Float prob = 1 / (1 + x);
            // Backward
            Float gradient, weight;
            if (label) {
                gradient = 2 * prob;
                weight = 1;
#ifdef USE_LOSS
                sample_loss += weight * -log(prob + kEpsilon);
#endif
            } else {
                gradient = -2 * prob / x;
                weight = negative_weight;
#ifdef USE_LOSS
                sample_loss += weight * -log(1 - prob + kEpsilon);
#endif
            }
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float h = head_buffer[i];
                Float t = tail[i];
                head_buffer[i] -= (optimizer.*update)(h, gradient * (h - t), weight);
                tail[i] -= (optimizer.*update)(t, gradient * (t - h), weight);
            }
        }
#ifdef USE_LOSS
        if (lane_id == 0)
            loss[sample_id] = sample_loss / (1 + num_negative * negative_weight);
#endif
        for (int i = lane_id; i < dim; i += kWarpSize)
            head[i] = head_buffer[i];
    }
}

template<class Vector, class Index, OptimizerType type>
__global__ void train_1_moment(Memory<Vector, Index> head_embeddings, Memory<Vector, Index> tail_embeddings,
                               Memory<Vector, Index> head_moment1s, Memory<Vector, Index> tail_moment1s,
                               Memory<Index, int> batch, Memory<Index, int> negative_batch, Optimizer optimizer,
                               float negative_weight
#ifdef USE_LOSS
        , Memory<typename Vector::Float, int> loss
#endif
) {
    static const size_t dim = Vector::dim;
    typedef typename Vector::Float Float;

    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int lane_id = thread_id % kWarpSize;
    int num_thread = gridDim.x * blockDim.x;
    int batch_size = batch.count / 2;
    int num_negative = negative_batch.count / batch_size;

    auto update = get_update_function_1_moment<Float, type>();

    __shared__ Vector buffer[kThreadPerBlock / kWarpSize];
    Vector &head_buffer = buffer[threadIdx.x / kWarpSize];

    for (int sample_id = thread_id / kWarpSize; sample_id < batch_size; sample_id += num_thread / kWarpSize) {
        // elements in std::tuple are stored in reverse order
        // each positive sample is {tail, head}
        Index head_id = batch[sample_id * 2 + 1];
        Vector &head = head_embeddings[head_id];
        Vector &head_moment1 = head_moment1s[head_id];
        for (int i = lane_id; i < dim; i += kWarpSize)
            head_buffer[i] = head[i];
#ifdef USE_LOSS
        Float sample_loss = 0;
#endif
        for (int s = 0; s <= num_negative; s++) {
            Index tail_id;
            int label;
            if (s < num_negative) {
                tail_id = negative_batch[sample_id * num_negative + s];
                label = 0;
            } else {
                tail_id = batch[sample_id * 2];
                label = 1;
            }
            Vector &tail = tail_embeddings[tail_id];
            Vector &tail_moment1 = tail_moment1s[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize)
                x += (head_buffer[i] - tail[i]) * (head_buffer[i] - tail[i]);
            x = WarpBroadcast(WarpReduce(x), 0) + kSmoothTerm;
            Float prob = 1 / (1 + x);
            // Backward
            Float gradient, weight;
            if (label) {
                gradient = 2 * prob;
                weight = 1;
#ifdef USE_LOSS
                sample_loss += weight * -log(prob + kEpsilon);
#endif
            } else {
                gradient = -2 * prob / x;
                weight = negative_weight;
#ifdef USE_LOSS
                sample_loss += weight * -log(1 - prob + kEpsilon);
#endif
            }
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float h = head_buffer[i];
                Float t = tail[i];
                head_buffer[i] -= (optimizer.*update)(h, gradient * (h - t),
                                                      head_moment1[i], weight);
                tail[i] -= (optimizer.*update)(t, gradient * (t - h),
                                               tail_moment1[i], weight);
            }
        }
#ifdef USE_LOSS
        if (lane_id == 0)
            loss[sample_id] = sample_loss / (1 + num_negative * negative_weight);
#endif
        for (int i = lane_id; i < dim; i += kWarpSize)
            head[i] = head_buffer[i];
    }
}

template<class Vector, class Index, OptimizerType type>
__global__ void train_2_moment(Memory<Vector, Index> head_embeddings, Memory<Vector, Index> tail_embeddings,
                               Memory<Vector, Index> head_moment1s, Memory<Vector, Index> tail_moment1s,
                               Memory<Vector, Index> head_moment2s, Memory<Vector, Index> tail_moment2s,
                               Memory<Index, int> batch, Memory<Index, int> negative_batch, Optimizer optimizer,
                               float negative_weight
#ifdef USE_LOSS
        , Memory<typename Vector::Float, int> loss
#endif
) {
    static const size_t dim = Vector::dim;
    typedef typename Vector::Float Float;

    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int lane_id = thread_id % kWarpSize;
    int num_thread = gridDim.x * blockDim.x;
    int batch_size = batch.count / 2;
    int num_negative = negative_batch.count / batch_size;

    auto update = get_update_function_2_moment<Float, type>();

    __shared__ Vector buffer[kThreadPerBlock / kWarpSize];
    Vector &head_buffer = buffer[threadIdx.x / kWarpSize];

    for (int sample_id = thread_id / kWarpSize; sample_id < batch_size; sample_id += num_thread / kWarpSize) {
        // elements in std::tuple are stored in reverse order
        // each positive sample is {tail, head}
        Index head_id = batch[sample_id * 2 + 1];
        Vector &head = head_embeddings[head_id];
        Vector &head_moment1 = head_moment1s[head_id];
        Vector &head_moment2 = head_moment2s[head_id];
        for (int i = lane_id; i < dim; i += kWarpSize)
            head_buffer[i] = head[i];
#ifdef USE_LOSS
        Float sample_loss = 0;
#endif
        for (int s = 0; s <= num_negative; s++) {
            Index tail_id;
            int label;
            if (s < num_negative) {
                tail_id = negative_batch[sample_id * num_negative + s];
                label = 0;
            } else {
                tail_id = batch[sample_id * 2];
                label = 1;
            }
            Vector &tail = tail_embeddings[tail_id];
            Vector &tail_moment1 = tail_moment1s[tail_id];
            Vector &tail_moment2 = tail_moment2s[tail_id];
            // Forward
            Float x = 0;
            for (int i = lane_id; i < dim; i += kWarpSize)
                x += (head_buffer[i] - tail[i]) * (head_buffer[i] - tail[i]);
            x = WarpBroadcast(WarpReduce(x), 0) + kSmoothTerm;
            Float prob = 1 / (1 + x);
            // Backward
            Float gradient, weight;
            if (label) {
                gradient = 2 * prob;
                weight = 1;
#ifdef USE_LOSS
                sample_loss += weight * -log(prob + kEpsilon);
#endif
            } else {
                gradient = -2 * prob / x;
                weight = negative_weight;
#ifdef USE_LOSS
                sample_loss += weight * -log(1 - prob + kEpsilon);
#endif
            }
            for (int i = lane_id; i < dim; i += kWarpSize) {
                Float h = head_buffer[i];
                Float t = tail[i];
                head_buffer[i] -= (optimizer.*update)(h, gradient * (h - t),
                                                      head_moment1[i], head_moment2[i], weight);
                tail[i] -= (optimizer.*update)(t, gradient * (t - h),
                                               tail_moment1[i], tail_moment2[i], weight);
            }
        }
#ifdef USE_LOSS
        if (lane_id == 0)
            loss[sample_id] = sample_loss / (1 + num_negative * negative_weight);
#endif
        for (int i = lane_id; i < dim; i += kWarpSize)
            head[i] = head_buffer[i];
    }
}
} // namespace largevis

namespace tsne{

} // namespace tsne

} // namespace visualization

} // namespace gpu
} // namespace graphvite