#pragma once

#include <type_traits>
#include "util/gpu.cuh"

namespace graphvite {

template<size_t _dim, class _Float = float>
class Vector {
//    static_assert(std::is_floating_point<_Float>::value, "Vector can only be instantiated with floating point types");
//    static_assert(_dim % gpu::kWarpSize == 0, "`dim` should be divided by 32");
public:
    static const size_t dim = _dim;
    typedef size_t Index;
    typedef _Float Float;
    Float data[dim];

    Vector() = default;
    Vector(Float f) {
        for (Index i = 0; i < dim; i++)
            data[i] = f;
    }

    __host__ __device__ Float &operator[](Index index) {
        return data[index];
    }

    __host__ __device__ Float operator[](Index index) const {
        return data[index];
    }

    Vector &operator =(Float f) {
#pragma unroll
        for (Index i = 0; i < dim; i++)
            data[i] = f;
        return *this;
    }

    Vector &operator +=(const Vector &v) {
#pragma unroll
        for (Index i = 0; i < dim; i++)
            data[i] += v[i];
        return *this;
    }

    Vector &operator -=(const Vector &v) {
#pragma unroll
        for (Index i = 0; i < dim; i++)
            data[i] -= v[i];
        return *this;
    }

    Vector &operator *=(const Vector &v) {
#pragma unroll
        for (Index i = 0; i < dim; i++)
            data[i] *= v[i];
        return *this;
    }

    Vector &operator /=(const Vector &v) {
#pragma unroll
        for (Index i = 0; i < dim; i++)
            data[i] /= v[i];
        return *this;
    }

    Vector &operator +=(Float f) {
#pragma unroll
        for (Index i = 0; i < dim; i++)
            data[i] += f;
        return *this;
    }

    Vector &operator -=(Float f) {
#pragma unroll
        for (Index i = 0; i < dim; i++)
            data[i] -= f;
        return *this;
    }

    Vector &operator *=(Float f) {
#pragma unroll
        for (Index i = 0; i < dim; i++)
            data[i] *= f;
        return *this;
    }

    Vector &operator /=(Float f) {
#pragma unroll
        for (Index i = 0; i < dim; i++)
            data[i] /= f;
        return *this;
    }

    Vector operator +(const Vector &v) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = (*this)[i] + v[i];
        return result;
    }

    Vector operator -(const Vector &v) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = (*this)[i] - v[i];
        return result;
    }

    Vector operator *(const Vector &v) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = (*this)[i] * v[i];
        return result;
    }

    Vector operator /(const Vector &v) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = (*this)[i] / v[i];
        return result;
    }

    Vector operator +(Float f) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = (*this)[i] + f;
        return result;
    }

    Vector operator -(Float f) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = (*this)[i] - f;
        return result;
    }

    Vector operator *(Float f) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = (*this)[i] * f;
        return result;
    }

    Vector operator /(Float f) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = (*this)[i] / f;
        return result;
    }

    friend Vector operator +(Float f, const Vector &v) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = v[i] + f;
        return result;
    }

    friend Vector operator -(Float f, const Vector &v) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = v[i] - f;
        return result;
    }

    friend Vector operator *(Float f, const Vector &v) {
        Vector result;
#pragma unroll
        for (Index i = 0; i < dim; i++)
            result[i] = v[i] * f;
        return result;
    }
};

} // namespace graphvite