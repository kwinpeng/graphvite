#include <string>
#include <vector>
#include <memory>
#include <sstream>
#include <iostream>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

#define USE_LOSS

#include "graph.h"
#include "solver.cuh"
#include "vector.h"

namespace py = pybind11;

namespace pybind11 {
typedef call_guard<gil_scoped_release> no_gil;
} // namespace pybind11

enum DType {
    float32 = 0,
    uint32,
    uint64
};

std::ostream& operator <<(std::ostream &out, DType dtype) {
    switch (dtype) {
        case float32:
            return out << typeid(float).name();
        case uint32:
            return out << typeid(unsigned int).name();
        case uint64:
            return out << typeid(unsigned long long).name();
    }
    return out;
}

template<class... Args>
std::string signature(std::string name, const Args &...args) {
    std::stringstream full_name;
    full_name << name << "_";
    auto _ = {(full_name << args, 0)...};
    return full_name.str();
}

template<class Solver>
#define Float typename Solver::Float
#define Vector typename Solver::Vector
std::function<py::array_t<Float>(Solver &)> numpy_view(std::shared_ptr<std::vector<Vector>> Solver::*field) {
#undef Vector
#undef Float
    const size_t dim = Solver::dim;
    typedef typename Solver::Float Float;

    return [field](Solver &solver) {
        Float *data = reinterpret_cast<Float *>((solver.*field)->data());
        py::capsule dummy(data, [](void *ptr) {});
        return py::array_t<Float>({(solver.*field)->size(), dim},
                                  {sizeof(Float) * dim, sizeof(Float)},
                                  data, dummy);
    };
}

// Class interface
template<class Index = size_t>
class pyGraph : public py::class_<graphvite::Graph<Index>> {
public:
    typedef graphvite::Graph<Index> Graph;
    typedef py::class_<Graph> Base;
    using Base::def_readonly;
    using Base::def;

    template<class... Args>
    pyGraph(py::handle scope, const char *name, const Args &...args) :
            Base(scope, signature(name, typeid(Index).name()).c_str(), args...) {
        // data members
        def_readonly("num_vertex", &Graph::num_vertex);
        def_readonly("num_edge", &Graph::num_edge);
        def_readonly("name2id", &Graph::name2id);
        def_readonly("id2name", &Graph::id2name);
        // member functions
        def(py::init<>(), py::no_gil());

        def("load", &Graph::load, py::no_gil(),
            py::arg("file_name"), py::arg("as_undirected") = true, py::arg("comment") = '#');

        def("save", &Graph::save, py::no_gil(),
            py::arg("file_name"), py::arg("weighted") = true, py::arg("anonymous") = false);
    }
};

template<class Index = size_t>
class pyWordGraph : public py::class_<graphvite::WordGraph<Index>, graphvite::Graph<Index>> {
public:
    typedef graphvite::WordGraph<Index> WordGraph;
    typedef py::class_<WordGraph, graphvite::Graph<Index>> Base;
    using Base::def_readonly;
    using Base::def;

    template<class... Args>
    pyWordGraph(py::handle scope, const char *name, const Args &...args) :
    Base(scope, signature(name, typeid(Index).name()).c_str(), args...) {
        // member functions
        def(py::init<>(), py::no_gil());

        def("load_compact", &WordGraph::load_compact, py::no_gil(),
            py::arg("file_name"), py::arg("window") = 5, py::arg("min_count") = 5, py::arg("comment") = '#');
    }
};

template<class Index = size_t>
class pyKnowledgeGraph : public py::class_<graphvite::KnowledgeGraph<Index>> {
public:
    typedef graphvite::KnowledgeGraph<Index> KnowledgeGraph;
    typedef py::class_<KnowledgeGraph> Base;
    using Base::def_readonly;
    using Base::def;

    template<class... Args>
    pyKnowledgeGraph(py::handle scope, const char *name, const Args &...args) :
            Base(scope, signature(name, typeid(Index).name()).c_str(), args...) {
        // data members
        def_readonly("num_vertex", &KnowledgeGraph::num_vertex);
        def_readonly("num_edge", &KnowledgeGraph::num_edge);
        def_readonly("num_relation", &KnowledgeGraph::num_relation);
        def_readonly("entity2id", &KnowledgeGraph::entity2id);
        def_readonly("relation2id", &KnowledgeGraph::relation2id);
        def_readonly("id2entity", &KnowledgeGraph::id2entity);
        def_readonly("entity2id", &KnowledgeGraph::entity2id);
        // member functions
        def(py::init<>(), py::no_gil());

        def("load", &KnowledgeGraph::load, py::no_gil(),
            py::arg("file_name"), py::arg("undirected_weight") = true, py::arg("comment") = '#');

        def("save", &KnowledgeGraph::save, py::no_gil(),
            py::arg("file_name"), py::arg("anonymous") = false);
    }
};

template<class Index = size_t>
class pyKNNGraph : public py::class_<graphvite::KNNGraph<Index>> {
public:
    typedef graphvite::KNNGraph<Index> KNNGraph;
    typedef py::class_<KNNGraph> Base;
    using Base::def_readonly;
    using Base::def;

    template<class... Args>
    pyKNNGraph(py::handle scope, const char *name, const Args &...args) :
            Base(scope, signature(name, typeid(Index).name()).c_str(), args...) {
        // data members
        def_readonly("num_vertex", &KNNGraph::num_vertex);
        def_readonly("num_edge", &KNNGraph::num_edge);

        // member functions
        def(py::init<int, int>(), py::no_gil(),
            py::arg("device_id") = 0, py::arg("num_thread") = 1);

        def("load_vector", &KNNGraph::load_vector, py::no_gil(),
            py::arg("file_name"), py::arg("num_neighbor") = 150, py::arg("perplexity") = 50, py::arg("comment") = '#');
    }
};

template<size_t dim, class Float = float, class Index = size_t>
class pyGraphSolver : public py::class_<graphvite::GraphSolver<dim, Float, Index>> {
public:
    typedef graphvite::GraphSolver<dim, Float, Index> GraphSolver;
    typedef py::class_<GraphSolver> Base;
    using Base::def_readonly;
    using Base::def_property_readonly;
    using Base::def;
    using Base::def_static;

    template<class... Args>
    pyGraphSolver(py::handle scope, const char *name, const Args &...args) :
            Base(scope, signature(name, dim, typeid(Float).name(), typeid(Index).name()).c_str(), args...) {
        // data members
        def_readonly("num_partition", &GraphSolver::num_partition);
        def_readonly("num_negative", &GraphSolver::num_negative);
        def_readonly("augmentation_step", &GraphSolver::augmentation_step);
        def_readonly("random_walk_length", &GraphSolver::random_walk_length);
        def_readonly("random_walk_batch_size", &GraphSolver::random_walk_batch_size);
        def_readonly("shuffle_base", &GraphSolver::shuffle_base);
        def_readonly("optimizer", &GraphSolver::optimizer);
        def_readonly("negative_sample_exponent", &GraphSolver::negative_sample_exponent);
        def_readonly("num_epoch", &GraphSolver::num_epoch);
        def_readonly("episode_size", &GraphSolver::episode_size);
        def_readonly("batch_size", &GraphSolver::batch_size);
        def_readonly("log_frequency", &GraphSolver::log_frequency);
        def_readonly("num_worker", &GraphSolver::num_worker);
        def_readonly("num_sampler", &GraphSolver::num_sampler);
        def_readonly("num_sampelr_per_worker", &GraphSolver::num_sampler_per_worker);
        def_readonly("gpu_memory_limit", &GraphSolver::gpu_memory_limit);

        // numpy views (mutable but unassignable)
        def_property_readonly("vertex_embeddings", numpy_view(&GraphSolver::vertex_embeddings));
        def_property_readonly("context_embeddings", numpy_view(&GraphSolver::context_embeddings));

        // member functions
        def(py::init<std::vector<int>, int, size_t>(), py::no_gil(),
            py::arg("device_ids") = std::vector<int>(), py::arg("num_sampler_per_worker") = 1,
            py::arg("gpu_memory_limit") = graphvite::kAuto);

        def("build", &GraphSolver::build, py::no_gil(),
            py::arg("graph"), py::arg("optimizer") = graphvite::Optimizer(graphvite::kAuto),
            py::arg("num_partition") = graphvite::kAuto, py::arg("num_negative") = 1, py::arg("batch_size") = 100000,
            py::arg("episode_size") = 250);

        def("train", &GraphSolver::train, py::no_gil(),
            py::arg("model") = "LINE", py::arg("num_epoch") = 100, py::arg("augmentation_step") = 5,
            py::arg("random_walk_length") = 40, py::arg("random_walk_batch_size") = 100,
            py::arg("shuffle_base") = graphvite::kAuto, py::arg("positive_reuse") = 1,
            py::arg("negative_sample_exponent") = 0.75, py::arg("negative_weight") = 5,
            py::arg("log_frequency") = 1000);

        // static functions
        def_static("gpu_memory_demand", &GraphSolver::gpu_memory_demand, py::no_gil(),
                   py::arg("num_vertex"), py::arg("num_worker") = 1, py::arg("num_partition") = 1,
                   py::arg("num_negative") = 1, py::arg("batch_size") = 100000);
    }
};

template<size_t dim, class Float = float, class Index = size_t>
class pyKnowledgeGraphSolver : public py::class_<graphvite::KnowledgeGraphSolver<dim, Float, Index>> {
public:
    typedef graphvite::KnowledgeGraphSolver<dim, Float, Index> KnowledgeGraphSolver;
    typedef py::class_<KnowledgeGraphSolver> Base;
    using Base::def_readonly;
    using Base::def_property_readonly;
    using Base::def;
    using Base::def_static;

    template<class... Args>
    pyKnowledgeGraphSolver(py::handle scope, const char *name, const Args &...args) :
            Base(scope, signature(name, dim, typeid(Float).name(), typeid(Index).name()).c_str(), args...) {
        // data members
        def_readonly("num_partition", &KnowledgeGraphSolver::num_partition);
        def_readonly("num_negative", &KnowledgeGraphSolver::num_negative);
        def_readonly("sample_batch_size", &KnowledgeGraphSolver::sample_batch_size);
        def_readonly("optimizer", &KnowledgeGraphSolver::optimizer);
        def_readonly("negative_sample_exponent", &KnowledgeGraphSolver::negative_sample_exponent);
        def_readonly("num_epoch", &KnowledgeGraphSolver::num_epoch);
        def_readonly("episode_size", &KnowledgeGraphSolver::episode_size);
        def_readonly("batch_size", &KnowledgeGraphSolver::batch_size);
        def_readonly("log_frequency", &KnowledgeGraphSolver::log_frequency);
        def_readonly("num_worker", &KnowledgeGraphSolver::num_worker);
        def_readonly("num_sampler", &KnowledgeGraphSolver::num_sampler);
        def_readonly("num_sampelr_per_worker", &KnowledgeGraphSolver::num_sampler_per_worker);
        def_readonly("gpu_memory_limit", &KnowledgeGraphSolver::gpu_memory_limit);

        // numpy views (mutable but unassignable)
        def_property_readonly("entity_embeddings", numpy_view(&KnowledgeGraphSolver::entity_embeddings));
        def_property_readonly("relation_embeddings", numpy_view(&KnowledgeGraphSolver::relation_embeddings));

        // member functions
        def(py::init<std::vector<int>, int, size_t>(), py::no_gil(),
            py::arg("device_ids") = std::vector<int>(), py::arg("num_sampler_per_worker") = 1,
            py::arg("gpu_memory_limit") = graphvite::kAuto);

        def("build", &KnowledgeGraphSolver::build, py::no_gil(),
            py::arg("graph"), py::arg("optimizer") = graphvite::Optimizer(graphvite::kAuto),
            py::arg("num_partition") = graphvite::kAuto, py::arg("num_negative") = 64, py::arg("batch_size") = 100000,
            py::arg("episode_size") = 1);

        def("train", &KnowledgeGraphSolver::train, py::no_gil(),
            py::arg("model") = "RotatE", py::arg("num_epoch") = 100, py::arg("margin") = 3,
            py::arg("sample_batch_size") = 2000, py::arg("positive_reuse") = 1, py::arg("adversarial_temperature") = 1,
            py::arg("log_frequency") = 1000);

        // static functions
        def_static("gpu_memory_demand", &KnowledgeGraphSolver::gpu_memory_demand, py::no_gil(),
                   py::arg("num_vertex"), py::arg("num_worker") = 1, py::arg("num_partition") = 1,
                   py::arg("num_negative") = 64, py::arg("batch_size") = 100000);
    }
};

template<size_t dim, class Float = float, class Index = size_t>
class pyVisualizationSolver : public py::class_<graphvite::VisualizationSolver<dim, Float, Index>> {
public:
    typedef graphvite::VisualizationSolver<dim, Float, Index> VisualizationSolver;
    typedef py::class_ <VisualizationSolver> Base;
    using Base::def_readonly;
    using Base::def_property_readonly;
    using Base::def;
    using Base::def_static;

    template<class... Args>
    pyVisualizationSolver(py::handle scope, const char *name, const Args &...args) :
            Base(scope, signature(name, dim, typeid(Float).name(), typeid(Index).name()).c_str(), args...) {
        // data members
        def_readonly("num_partition", &VisualizationSolver::num_partition);
        def_readonly("num_negative", &VisualizationSolver::num_negative);
        def_readonly("optimizer", &VisualizationSolver::optimizer);
        def_readonly("negative_sample_exponent", &VisualizationSolver::negative_sample_exponent);
        def_readonly("num_epoch", &VisualizationSolver::num_epoch);
        def_readonly("episode_size", &VisualizationSolver::episode_size);
        def_readonly("batch_size", &VisualizationSolver::batch_size);
        def_readonly("log_frequency", &VisualizationSolver::log_frequency);
        def_readonly("num_worker", &VisualizationSolver::num_worker);
        def_readonly("num_sampler", &VisualizationSolver::num_sampler);
        def_readonly("num_sampelr_per_worker", &VisualizationSolver::num_sampler_per_worker);
        def_readonly("gpu_memory_limit", &VisualizationSolver::gpu_memory_limit);

        // numpy views (mutable but unassignable)
        def_property_readonly("coordinates", numpy_view(&VisualizationSolver::coordinates));

        // member functions
        def(py::init<std::vector<int>, int, size_t> (), py::no_gil(),
            py::arg("device_ids") = std::vector<int>(), py::arg("num_sampler_per_worker") = 1,
            py::arg("gpu_memory_limit") = graphvite::kAuto);

        def("build", &VisualizationSolver::build, py::no_gil(),
            py::arg("graph"), py::arg("optimizer") = graphvite::Optimizer(graphvite::kAuto),
            py::arg("num_partition") = graphvite::kAuto, py::arg("num_negative") = 1, py::arg("batch_size") = 100000,
            py::arg("episode_size") = 250);

        def("train", &VisualizationSolver::train, py::no_gil(),
            py::arg("model") = "LargeVis", py::arg("num_epoch") = 100, py::arg("sample_batch_size") = 2000,
            py::arg("positive_reuse") = 1, py::arg("negative_sample_exponent") = 0.75, py::arg("negative_weight") = 35,
            py::arg("log_frequency") = 1000);

        // static functions
        def_static("gpu_memory_demand", &VisualizationSolver::gpu_memory_demand, py::no_gil(),
                   py::arg("num_vertex"), py::arg("num_worker") = 1, py::arg("num_partition") = 1,
                   py::arg("num_negative") = 1, py::arg("batch_size") = 100000);
    }
};

template <class... DTypes>
std::function<py::object(DTypes..., py::args, py::kwargs)> type_helper(
        const py::module &module, const std::string &name) {
    return [module, name](const DTypes &...dtypes, py::args args, py::kwargs kwargs) {
        auto class_name = signature(name, dtypes...);
        return module.attr(py::str(class_name))(*args, **kwargs);
    };
}

// Optimizer interface
class pyOptimizer : public py::class_<graphvite::Optimizer> {
public:
    typedef graphvite::Optimizer Optimizer;
    typedef py::class_<Optimizer> Base;
    using Base::def_readonly;
    using Base::def;

    template<class... Args>
    pyOptimizer(py::handle scope, const char *name, const Args &...args) :
            Base(scope, name, args...) {
        // data members
        def_readonly("type", &Optimizer::type);
        def_readonly("lr", &Optimizer::lr);
        def_readonly("weight_decay", &Optimizer::weight_decay);

        // member functions
        def(py::init<int>(), py::no_gil(),
            py::arg("type") = graphvite::kAuto);
        def(py::init<float>(), py::no_gil(),
            py::arg("lr") = 1e-4);
    }
};

class pySGD : public py::class_<graphvite::SGD, graphvite::Optimizer> {
public:
    typedef graphvite::SGD SGD;
    typedef py::class_<SGD, graphvite::Optimizer> Base;
    using Base::def_readonly;
    using Base::def;

    template<class... Args>
    pySGD(py::handle scope, const char *name, const Args &...args) :
            Base(scope, name, args...) {
        // member functions
        def(py::init<float, float>(), py::no_gil(),
            py::arg("lr") = 1e-4, py::arg("weight_decay") = 0);
    }
};

class pyMomentum : public py::class_<graphvite::Momentum, graphvite::Optimizer> {
public:
    typedef graphvite::Momentum Momentum;
    typedef py::class_<Momentum, graphvite::Optimizer> Base;
    using Base::def_readonly;
    using Base::def;

    template<class... Args>
    pyMomentum(py::handle scope, const char *name, const Args &...args) :
            Base(scope, name, args...) {
        // data members
        def_readonly("momentum", &Momentum::momentum);

        // member functions
        def(py::init<float, float, float>(), py::no_gil(),
            py::arg("lr") = 1e-4, py::arg("weight_decay") = 0, py::arg("momentum") = 0.9);
    }
};

class pyAdam : public py::class_<graphvite::Adam, graphvite::Optimizer> {
public:
    typedef graphvite::Adam Adam;
    typedef py::class_<Adam, graphvite::Optimizer> Base;
    using Base::def_readonly;
    using Base::def;

    template<class... Args>
    pyAdam(py::handle scope, const char *name, const Args &...args) :
            Base(scope, name, args...) {
        // data members
        def_readonly("beta1", &Adam::beta1);
        def_readonly("beta2", &Adam::beta2);
        def_readonly("epsilon", &Adam::epsilon);

        // member functions
        def(py::init<float, float, float, float, float>(), py::no_gil(),
            py::arg("lr") = 1e-4, py::arg("weight_decay") = 0, py::arg("beta1") = 0.999, py::arg("beta2") = 0.99999,
            py::arg("epsilon") = 1e-8);
    }
};

std::function<py::object(std::string, py::args, py::kwargs)> optimizer_helper(const py::module &module) {
    return [module](const std::string type, py::args args, py::kwargs kwargs) {
        return module.attr(py::str(type))(*args, **kwargs);
    };
}
